//--------------------------------------------------------
// OpenNero : IOEnums
//  All the enumerations for input
//--------------------------------------------------------

#ifndef _INPUT_IOENUMS_H_
#define _INPUT_IOENUMS_H_

namespace OpenNero 
{
    /// Currently we only accept input via Irrlicht
    typedef irr::EKEY_CODE KEY;

    /// All mouse buttons
    enum MOUSE_BUTTON
    {
        MOUSE_LBUTTON = 0x0001,
        MOUSE_MBUTTON,
        MOUSE_RBUTTON,

        // pseudo buttons
        MOUSE_MOVE_X,
        MOUSE_MOVE_Y,
        MOUSE_SCROLL,

        MOUSE_BUTTONS_COUNT
    };

	enum JOYSTICK_BUTTON 
	{
		JOYSTICK_1 = 0x0001,
		JOYSTICK_2,
		JOYSTICK_3,
		JOYSTICK_4,
		JOYSTICK_5,
		JOYSTICK_6,
		JOYSTICK_7,
		JOYSTICK_8,
		JOYSTICK_9,
		JOYSTICK_10,
		JOYSTICK_11,
		JOYSTICK_12,
		JOYSTICK_13,
		JOYSTICK_14,
		JOYSTICK_15,
		JOYSTICK_16,
		JOYSTICK_17,
		JOYSTICK_18,
		JOYSTICK_19,
		JOYSTICK_20,
		JOYSTICK_21,
		JOYSTICK_22,
		JOYSTICK_23,
		JOYSTICK_24,
		JOYSTICK_25,
		JOYSTICK_26,
		JOYSTICK_27,
		JOYSTICK_28,
		JOYSTICK_29,
		JOYSTICK_30,
		JOYSTICK_31,
		JOYSTICK_32,

		// pseudo buttons
		JOYSTICK_MOVE_X,
		JOYSTICK_MOVE_Y,
		JOYSTICK_MOVE_Z,
		JOYSTICK_MOVE_R,
		JOYSTICK_MOVE_U,
		JOYSTICK_MOVE_V,

		JOYSTICK_BUTTONS_COUNT
	};

	enum JoystickAxis {
		AXIS_X = 0, // e.g. analog stick 1 left to right
		AXIS_Y,		// e.g. analog stick 1 top to bottom
		AXIS_Z,		// e.g. throttle, or analog 2 stick 2 left to right
		AXIS_R,		// e.g. rudder, or analog 2 stick 2 top to bottom
		AXIS_U,
		AXIS_V
	};
}; // end OpenNero


#endif // _INPUT_IOENUMS_H_
