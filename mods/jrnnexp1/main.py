#!/usr/bin/python
# -*- coding: utf-8 -*-
import OpenNero
import common
import common.menu_utils
from jrnnexp1.environment import *
from jrnnexp1.agent import *
#from jrnnexp1.ObsUtility import UserSettings
from jrnnexp1.UserSettings import UserSettings as UserSettings
import jrnnexp1.client
import jrnnexp1.module
import time
import math
import re
import os
import jrnnexp1.wingdbstub




def ModMain():
    jrnnexp1.client.ClientMain()


def OldMain():
    print 'Hello, Mod!'
    addObject('data/cube/Cube.xml', Vector3f(0, 0, 0))
    addObject('data/terrain/GrassyTerrain.xml', Vector3f(-50, -50, 0))
    addObject('data/wall/BrickWall.xml', Vector3f(-50, 0, 0),
              Vector3f(0, 0, 90), Vector3f(5, 1, 1))
    addObject('data/wall/BrickWall.xml', Vector3f(50, 0, 0),
              Vector3f(0, 0, 90), Vector3f(5, 1, 1))
    addObject('data/wall/BrickWall.xml', Vector3f(0, -50, 0),
              Vector3f(0, 0, 0), Vector3f(5, 1, 1))
    addObject('data/wall/BrickWall.xml', Vector3f(0, 50, 0),
              Vector3f(0, 0, 0), Vector3f(5, 1, 1))

    # addObject("data/wall/BrickWall.xml", Vector3f(0,0,0), Vector3f(0,0,0), Vector3f(1,1,1))

    camRotateSpeed = 10
    camMoveSpeed = 1000
    camZoomSpeed = 400
    cam = getSimContext().addCamera(camRotateSpeed, camMoveSpeed,
                                    camZoomSpeed)
    cam.setFarPlane(5000)
    cam.setEdgeScroll(False)
    cam.setPosition(Vector3f(30, 30, 30))
    cam.setTarget(Vector3f(0, 0, 1))

    # create an environment for the agent

    env = TestEnvironment()
    set_environment(env)

    # Create key binding to control the agent

    ioMap = createInputMapping()
    getSimContext().setInputMapping(ioMap)

    # enable AI and create agent

    enable_ai()
    addObject('data/character/SydneyAgent.xml', Vector3f(0, 0, 4.5))


script_server = None
elapsedTime = 0
lastTime = time.clock()
headless = False


def ModTick(dt):
    global script_server
    global elapsedTime
    global lastTime
    global headless
    curTime = time.clock()
    change = (curTime - lastTime)
    lastTime = curTime
    elapsedTime += change
    # print elapsedTime
    jrnnexp1.module.getMod().keepTime(math.floor(elapsedTime * 100) / 100.0)
    # if elapsedTime % 5 == 0:
    #     print "Time!"

    if OpenNero.getAppConfig().rendertype == 'null':
        return

    if script_server is None and not headless:
        script_server = common.menu_utils.GetScriptServer()
        common.startScript('jrnnexp1/menu.py')

    
    if not headless:
        data = script_server.read_data()

        while data:
            jrnnexp1.module.parseInput(data.strip())
            data = script_server.read_data()

        if jrnnexp1.module.getMod().doSendEndMsg():
            script_server.send_data("End")


def TestNetwork(netfilename, resultfile, numRepeats=5, numTicks=2000):
    global headless
    headless = True
    fileParams = re.compile('(.*)-(.+ - .+?)-')
    mod = jrnnexp1.module.getMod()
    mod.set_speedup(100)
    params = fileParams.findall(netfilename)[0]
    user = params[0]
    scene = params[1]
    epsNum = 1
    users_path = os.path.join(os.curdir, "jrnnexp1", "users")
    userP = os.path.join(users_path, user)
    mod.setUserInfo(user, userP)
    mod.numEpisodes = numRepeats
    paramString = "{scene},{epsNum},{netfilename},{numTicks},{resultfile}".format(**locals())
    mod.testNetFromFile(paramString)

    print "Testing Network: {netfilename}".format(**locals())
    