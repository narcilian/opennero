import OpenNero
import common
import constants
import environment
import sys
import os
import re
import time
import json
import PyJRNN
import numpy
import pyublas
import ObsUtility
import cPickle as pickle
#from ObsUtility import UserSettings
from random import random, choice
from UserSettings import UserSettings as UserSettings
#class UserSettings:
    #def __init__(self, turnSensitivity=30, deadzone=15, speedup=100):
        #self.turnSensitivity = turnSensitivity
        #self.deadzone = deadzone
        #self.speedup = speedup

def jrnnexp1_rewards():
    """
    Reward for this experiment
    """
    reward = OpenNero.FeatureVectorInfo()
    for f in constants.FITNESS_DIMENSIONS:
        reward.add_continuous(-sys.float_info.max, sys.float_info.max)
    return reward


class Timer:
    """
    Class for creating timers with callback functions
    """
    def __init__(self, startTime=None, Time=None, callback=None, Ticks=None, realTime=False, repeat=False):
        """Initialize the timer class

        Parameters
        ----------
        startTime - The time this timer was created. This is set from the signal from the simulation
        Time - How long the timer should wait to call the callback functions
        callback - Function that should be called when the timer completes
        repeat - Should the timer be reinstated when it has completed.
        """
        self.startTime = startTime if realTime == False else time.clock()
        self.Time = Time
        self.callback = callback
        self.repeat = repeat
        self.Ticks = Ticks
        self.TickCount = 0
        self.realTime = realTime

    def Tick(self):
        if self.realTime == False:
            if self.Ticks == None:
                return False
            elif self.Ticks == self.TickCount:
                return True
            else:
                self.TickCount += 1
                return False
        else:
            return True if time.clock() - self.startTime > self.Time else False

    def ResetTick(self):
        self.TickCount = 0
        if self.realTime:
            self.startTime = time.clock()


class JRNNExp1Module:
    def __init__(self):
        self.verbose = True
        self.environment = None
        self.agent_id = None
        self.curr_team = constants.OBJECT_TYPE_TEAM_0
        self.io_map = None
        self.turn_sensitivity = 100
        self.userPath = ""
        self.dataPath = ""
        self.userName = ""
        self.historyPath = ""
        self.trainingPath = ""
        self.agentPath = ""
        self.resultPath = ""
        self.currScenarioName = ""
        self.currScenario = ""
        self.currSpeedUp = constants.DEFAULT_SPEEDUP
        #self.set_speedup(constants.DEFAULT_SPEEDUP)
        self.ObstacleIds = []
        self.AgentIds = []
        self.AgentNetworks = {}
        self.replayScenarioFiles = {}
        self.episodeTime = 30  # The time in seconds for an episode
        self.episodeCounter = 0
        self.numEpisodes = 10  # Numver of episodes will be configurable later
        self.restTime = 10
        self.sendEndMsg = False
        self.elapsedTime = 0
        self.timers = []
        self.agentsLoaded = False
        self.obstaclesLoaded = False
        self.scenarioEnvironmentLoaded = False
        self.practice = False
        self.ReplayList = None

        x = constants.XDIM / 2.0
        y = constants.YDIM / 2.0
        self.spawn_x = {}
        self.spawn_y = {}
        self.set_spawn(x, y, constants.OBJECT_TYPE_TEAM_0)
        self.set_spawn(x, y, constants.OBJECT_TYPE_TEAM_1)

    def setup_map(self):
        """
        setup the test environment
        """
        OpenNero.disable_ai()

        if self.environment:
            error("Environment already created")
            return

        addObstacles = False

        # create the environment
        self.environment = self.create_environment()
        OpenNero.set_environment(self.environment)

        # world walls
        height = constants.HEIGHT + constants.OFFSET
        common.addObject(
            "data/shapes/cube/Cube.xml",
            OpenNero.Vector3f(constants.XDIM / 2, 0, height),
            OpenNero.Vector3f(0, 0, 90),
            scale=OpenNero.Vector3f(
                constants.WIDTH, constants.XDIM, constants.HEIGHT * 2),
            label="World Wall0",
            type=constants.OBJECT_TYPE_OBSTACLE)
        common.addObject(
            "data/shapes/cube/Cube.xml",
            OpenNero.Vector3f(0, constants.YDIM / 2, height),
            OpenNero.Vector3f(0, 0, 0),
            scale=OpenNero.Vector3f(
                constants.WIDTH, constants.YDIM, constants.HEIGHT * 2),
            label="World Wall1",
            type=constants.OBJECT_TYPE_OBSTACLE)
        common.addObject(
            "data/shapes/cube/Cube.xml",
            OpenNero.Vector3f(constants.XDIM, constants.YDIM / 2, height),
            OpenNero.Vector3f(0, 0, 0),
            scale=OpenNero.Vector3f(
                constants.WIDTH, constants.YDIM, constants.HEIGHT * 2),
            label="World Wall2",
            type=constants.OBJECT_TYPE_OBSTACLE)
        common.addObject(
            "data/shapes/cube/Cube.xml",
            OpenNero.Vector3f(constants.XDIM / 2, constants.YDIM, height),
            OpenNero.Vector3f(0, 0, 90),
            scale=OpenNero.Vector3f(
                constants.WIDTH, constants.XDIM, constants.HEIGHT * 2),
            label="World Wall3",
            type=constants.OBJECT_TYPE_OBSTACLE)

        if addObstacles:
            self.addObstacles()

        # Add the surrounding Environment
        common.addObject(
            "data/terrain/NeroWorld.xml",
            OpenNero.Vector3f(constants.XDIM / 2, constants.YDIM / 2, 0),
            scale=OpenNero.Vector3f(1.2, 1.2, 1.2),
            label="NeroWorld",
            type=constants.OBJECT_TYPE_LEVEL_GEOM)

        return True

    def set_spawn(self, x, y, team=constants.OBJECT_TYPE_TEAM_0):
        self.spawn_x[team] = x
        self.spawn_y[team] = y

    def set_speedup(self, speedup):
        self.currSpeedUp = speedup
        OpenNero.getSimContext().delay = 1.0 - (1.0 - 0.015 * ((100.0 - speedup) / 100.0))
        print 'speedup delay', OpenNero.getSimContext().delay
        if self.environment:
            self.environment.speedup = speedup / 100.0

    def loadScenarioFiles(self, scenarioPath):
        scenariofiles = {}
        trainingdir = os.path.join(scenarioPath, "training")
        historydir = os.path.join(scenarioPath, "history")
        scenariofiles["training"] = [os.path.join(trainingdir, x) for x in os.listdir(trainingdir)]
        scenariofiles["history"] = [os.path.join(historydir, x) for x in os.listdir(historydir)]
        return scenariofiles

    def getSceneEpsNames(self, scene):
        scenePath = os.path.join(self.dataPath, scene)
        tmpDir = os.path.join(scenePath, "history")
        epsNum = re.compile('eps_(\d+)')
        fileNames = sorted([os.path.basename(x) for x in os.listdir(tmpDir) if 'eps' in x], key=lambda x: int(epsNum.search(x).group(1)))
        epsNames = ['Episode {0}'.format(epsNum.search(x).group(1)) for x in fileNames]
        return epsNames

    def getSceneAgents(self, scene):
        agentlist = []
        userAgentP = self.agentPath
        agentlist = [x for x in os.listdir(userAgentP) if scene in x]
        return agentlist

    def LoadUserData(self):
        # global userspath
        Scenarios = {}
        # userP = os.path.join(self._usersPath, username)
        # userDataP = os.path.join(userP, "data")
        scenarioList = os.listdir(self.dataPath)
        for scene in scenarioList:
            Scenarios[scene] = self.loadScenarioFiles(os.path.join(self.dataPath, scene))

        return Scenarios

    def create_environment(self):
        return environment.JRNNEnvironment()

    def load_agents(self):
        if self.agentsLoaded == False:
            OpenNero.enable_ai()
            common.addObject("data/character/steve_red_rtneat.xml", OpenNero.Vector3f(constants.XDIM / 4, constants.YDIM / 2, 0.2), type=constants.OBJECT_TYPE_TEAM_0)
            common.addObject("data/character/steve_blue_rtneat.xml", OpenNero.Vector3f(3 * constants.XDIM / 4, constants.YDIM / 2, 0.2), type=constants.OBJECT_TYPE_TEAM_1)
            self.agentsLoaded = True

    def load_chase_agents(self):
        if self.agentsLoaded == False:
            print "Loading Runner Agent & Person"
            # OpenNero.enable_ai()
            point1, point2 = self.getSpawnPoints()
            Id = common.addObject("data/character/steve_red_rtneat.xml", OpenNero.Vector3f(point1[0], point1[1], 0.2), type=constants.OBJECT_TYPE_TEAM_0)
            self.AgentIds.append(Id)
            Id = common.addObject("data/character/steve_blue_rtneat.xml", OpenNero.Vector3f(point2[0], point2[1], 0.2), type=constants.OBJECT_TYPE_TEAM_1)
            self.AgentIds.append(Id)
            self.agentsLoaded = True

    def load_network_agents(self, trainingFilePath="", networkFilePath="", useFannedOut=True, saveNetToFile="", chase=True):
        if self.agentsLoaded == False:
            print "Loading Runner & Network Agent"
            point1, point2 = self.getSpawnPoints()
            while self.environment.distance(point1, point2) > 200:
                point1, point2 = self.getSpawnPoints()
            self.spawnNetworkAgent(point1, trainingFilePath=trainingFilePath, networkFilePath=networkFilePath, useFannedOut=useFannedOut, saveNetToFile=saveNetToFile)
            if chase:
                Id = common.addObject("data/character/steve_blue_rtneat.xml", OpenNero.Vector3f(point2[0], point2[1], 0.2), type=constants.OBJECT_TYPE_TEAM_1)
                self.AgentIds.append(Id)
            self.agentsLoaded = True

    def load_stationary_agents(self):
        if self.agentsLoaded == False:
            print "Loading Stationary Agent & Person"
            # OpenNero.enable_ai()
            point1, point2 = self.getSpawnPoints()
            Id = common.addObject("data/character/steve_red_rtneat.xml", OpenNero.Vector3f(point1[0], point1[1], 0.2), type=constants.OBJECT_TYPE_TEAM_0)
            self.AgentIds.append(Id)
            #common.addObject("data/character/steve_stationary.xml", OpenNero.Vector3f(point2[0], point2[1], 0.2), type=constants.OBJECT_TYPE_TEAM_1)
            self.agentsLoaded = True

    def load_replay_agents(self, filePath=None):
        if self.agentsLoaded == False:
            print "Loading Replay Agent(s)"
            with open(filePath, 'r') as fileh:
                history = json.load(fileh)
                if 'Runner' in history:
                    print "Loading Runner Agent"
                    init_pos = history['Runner'][0]['initial_position']
                    self.curr_team = constants.OBJECT_TYPE_TEAM_1
                    Id = common.addObject('data/character/steve_blue_replay.xml', OpenNero.Vector3f(float(init_pos['x']), float(init_pos['y']), float(init_pos['z'])), type=constants.OBJECT_TYPE_TEAM_1)
                    self.environment.agents_to_load[Id] = {'agentname': 'Runner', 'filePath': filePath}
                    self.AgentIds.append(Id)

                print "Loading Person Agent"
                init_pos = history['Person'][0]['initial_position']
                historyLength = len(history['Person']) - 1
                self.curr_team = constants.OBJECT_TYPE_TEAM_0
                Id = common.addObject('data/character/steve_blue_replay.xml', OpenNero.Vector3f(float(init_pos['x']), float(init_pos['y']), float(init_pos['z'])), type=constants.OBJECT_TYPE_TEAM_1)
                self.environment.agents_to_load[Id] = {'agentname': 'Person', 'filePath': filePath}
                self.AgentIds.append(Id)

            self.agentsLoaded = True
            return historyLength

    def kill_agents(self):
        print "Killing Agents"
        self.environment.kill_all_agents(constants.OBJECT_TYPE_TEAM_0)
        self.environment.kill_all_agents(constants.OBJECT_TYPE_TEAM_1)

    def remove_agents(self):
        if self.agentsLoaded == True:
            print "Removing Agents"
            self.environment.remove_all_agents(constants.OBJECT_TYPE_TEAM_0)
            self.environment.remove_all_agents(constants.OBJECT_TYPE_TEAM_1)
            #OpenNero.disable_ai()
            self.agentsLoaded = False

    def addObstacles(self, filePath=None):
        # Add an obstacle wall in the middle
        print "Loading Obstacles"
        if self.obstaclesLoaded == False:
            Id = common.addObject(
                "data/shapes/cube/Cube.xml",
                OpenNero.Vector3f(constants.XDIM / 2, constants.YDIM / 2, constants.HEIGHT + constants.OFFSET),
                OpenNero.Vector3f(0, 0, 90),
                scale=OpenNero.Vector3f(constants.WIDTH, constants.YDIM / \
                                        4, constants.HEIGHT * 2),
                label="World Wall4",
                type=constants.OBJECT_TYPE_OBSTACLE)
            self.ObstacleIds.append(Id)
            # Add some trees
            # for i in (0.25, 0.75):
            #     for j in (0.25, 0.75):
            if filePath == None:
                for i in range(1, 10, 1):
                    i = i / 10.0
                    i = i + ((random() * 0.10) - 0.05)
                    for j in range(1, 10, 1):
                        # don't collide with trees - they are over 500 triangles each
                        # Add some randomness to the placement.
                        j = j / 10.0
                        j = j + ((random() * 0.10) - 0.05)
                        Id = common.addObject(
                            "data/shapes/tree/Tree.xml",
                            OpenNero.Vector3f(i * constants.XDIM,
                                              j * constants.YDIM, constants.OFFSET),
                            OpenNero.Vector3f(0, 0, 0),
                            scale=OpenNero.Vector3f(1, 1, 1),
                            label="Tree %d %d" % (10 * i, 10 * j),
                            type=constants.OBJECT_TYPE_LEVEL_GEOM)
                        self.ObstacleIds.append(Id)
                        # collide with their trunks represented with cubes instead
                        Id = common.addObject(
                            "data/shapes/cube/Cube.xml",
                            OpenNero.Vector3f(i * constants.XDIM,
                                              j * constants.YDIM, 0),
                            OpenNero.Vector3f(0, 0, 0),
                            scale=OpenNero.Vector3f(2, 2, 2),
                            type=constants.OBJECT_TYPE_OBSTACLE)
                        self.ObstacleIds.append(Id)
                self.obstaclesLoaded = True
                if self.historyPath != "" and not self.practice:
                    filepath = os.path.join(self.historyPath, "Obstacles.json")
                    fileh = open(filepath, "w")
                    self.saveObstaclesToFile(fileh)
                    fileh.close()
            else:
                with open(filePath, 'r') as fileh:
                    print filePath
                    savedObstacles = json.load(fileh)
                    trees = [x for x in savedObstacles['Obstacles'] if 'Tree' in x['label']]
                    for tree in trees:
                        Id = common.addObject(
                            "data/shapes/tree/Tree.xml",
                            OpenNero.Vector3f(float(tree['position']['x']),
                                              float(tree['position']['y']), float(tree['position']['z'])),
                            OpenNero.Vector3f(0, 0, 0),
                            scale=OpenNero.Vector3f(1, 1, 1),
                            label=tree['label'].encode('ascii', 'ignore'),
                            type=constants.OBJECT_TYPE_LEVEL_GEOM)
                        self.ObstacleIds.append(Id)
                        Id = common.addObject(
                            "data/shapes/cube/Cube.xml",
                            OpenNero.Vector3f(float(tree['position']['x']),
                                              float(tree['position']['y']), 0),
                            OpenNero.Vector3f(0, 0, 0),
                            scale=OpenNero.Vector3f(2, 2, 2),
                            type=constants.OBJECT_TYPE_OBSTACLE)
                        self.ObstacleIds.append(Id)
                    self.obstaclesLoaded = True

    def removeObstacles(self):
        if self.obstaclesLoaded == True:
            print "Removing Obstacles"
            for Id in self.ObstacleIds:
                common.removeObject(Id)
            del self.ObstacleIds[:]
            self.obstaclesLoaded = False

    def saveObstaclesToFile(self, fileh):
        obstacles = []
        for Id in self.ObstacleIds:
            position = OpenNero.getSimContext().getObjectPosition(Id)
            label = OpenNero.getSimContext().getObjectLabel(Id)
            obstacles.append({"label": label, "position": {"x": position.x, "y": position.y, "z": position.z}})
        json.dump({"Obstacles": obstacles}, fileh)

    def createScenarioPaths(self):
        currTime = time.strftime("%d%b%Y%H%M%S")
        self.currScenarioName = "{0} - {1}".format(self.currScenario, currTime)
        tmpPath = os.path.join(self.dataPath, self.currScenarioName)
        os.mkdir(tmpPath)
        self.historyPath = os.path.join(tmpPath, "history")
        self.trainingPath = os.path.join(tmpPath, "training")
        os.mkdir(self.historyPath)
        os.mkdir(self.trainingPath)

    def clearScenarioPaths(self):
        self.currScenarioName = ""
        self.historyPath = ""
        self.trainingPath = ""

    def startScenario(self, scenarioName="", practice=False):
        if scenarioName != "":
            print "Starting Scenario: ", scenarioName.encode('ascii', 'ignore')
            self.currScenario = scenarioName.encode('ascii', 'ignore')
            self.episodeCounter = 0
            if not practice:
                self.createScenarioPaths()
            if "Shoot" in self.currScenario:
                constants.nextDisplayHint()

        OpenNero.enable_ai()
        self.practice = practice
        if self.currScenario == "":
            return

        if "Chase" in self.currScenario:
            self.load_chase_agents()
        else:
            self.load_stationary_agents()

        if "Obstacles" in self.currScenario:
            self.addObstacles()

        if "Shoot" in self.currScenario:
            self.environment.allowShooting = True
        else:
            self.environment.allowShooting = False

        if not practice:
            self.episodeCounter += 1
            print "Starting Episode: ", self.episodeCounter
            if self.episodeCounter >= self.numEpisodes:
                self.timers.append(Timer(startTime=self.elapsedTime, Time=self.episodeTime, callback=self.endScenario))
            else:
                self.timers.append(Timer(startTime=self.elapsedTime, Time=self.episodeTime, callback=self.resetScenario))

    def resetScenario(self):
        print "Reseting"
        self.kill_agents()
        OpenNero.disable_ai()
        self.write_agent_data()
        self.remove_agents()
        self.timers.append(Timer(startTime=self.elapsedTime, Time=self.restTime, callback=self.startScenario))

    def endScenario(self, sendEnd=True):
        print "Ending Scenario: ", self.currScenario

        if "Shoot" in self.currScenario:
            constants.nextDisplayHint()

        self.kill_agents()
        OpenNero.disable_ai()
        if not self.practice:
            self.write_agent_data()
        self.remove_agents()
        self.removeObstacles()
        self.episodeCounter = 0
        self.currScenario = ""
        self.clearTimers()
        self.clearScenarioPaths()
        if sendEnd == True:
            self.sendEndMsg = True

    def endReplay(self, sendEnd=True):
        print "Ending Scenario: ", self.currScenario

        if "Shoot" in self.currScenario:
            constants.nextDisplayHint()

        self.kill_agents()
        OpenNero.disable_ai()
        self.set_speedup(self.resetSpeedUpSpeed)
        if not self.practice:
            self.write_agent_data()
        self.remove_agents()
        self.removeObstacles()
        self.episodeCounter = 0
        self.currScenario = ""
        self.clearTimers()
        self.clearScenarioPaths()
        self.scenarioEnvironmentLoaded = False
        if sendEnd == True:
            self.sendEndMsg = True

    def replayScenario(self, sceneParameters=""):
        # print sceneParameters
        params = sceneParameters.split(',')
        sceneName = params[0]
        print 'Starting Replay of {0}'.format(sceneName)
        self.currScenario = sceneName
        epiNum = int(params[1].split(' ')[1])
        self.replayScenarioFiles = self.LoadUserData()
        self.practice = True
        self.resetSpeedUpSpeed = self.currSpeedUp
        #self.set_speedup(constants.DEFAULT_SPEEDUP)
        OpenNero.enable_ai()
        numTicks = self.loadReplayEnvironment(sceneName, epiNum)
        self.timers.append(Timer(Ticks=numTicks, callback=self.endReplay))

    def startReplayOrAgent(self):
        paramstuple = next(self.ReplayList, ('Done', ''))
        if paramstuple[0] == 'Replay':
            sceneParameters = paramstuple[1]
            params = sceneParameters.split(',')
            sceneName = params[0]
            print 'Starting Replay of {0}'.format(sceneName)
            self.currScenario = sceneName
            epiNum = int(params[1].split(' ')[1])
            self.currEpisode = epiNum
            self.replayScenarioFiles = self.LoadUserData()
            self.practice = True
            self.resetSpeedUpSpeed = self.currSpeedUp
            # self.set_speedup(constants.DEFAULT_SPEEDUP)
            OpenNero.enable_ai()
            numTicks = self.loadReplayEnvironment(sceneName, epiNum)
            self.timers.append(Timer(Time=self.episodeTime, realTime=True, callback=self.resetReplay))
            # self.timers.append(Timer(Ticks=numTicks, callback=self.resetReplay))

        elif paramstuple[0] == 'Agent':
            sceneParameters = paramstuple[1]
            params = sceneParameters.split(',')
            sceneName = params[0]
            self.currScenario = sceneName
            epiNum = int(params[1].split(' ')[1])
            agentFileName = params[2]
            self.replayScenarioFiles = self.LoadUserData()
            self.practice = True
            self.resetSpeedUpSpeed = self.currSpeedUp
            #self.set_speedup(constants.DEFAULT_SPEEDUP)
            OpenNero.enable_ai()

            self.loadScenarioEnvironment(sceneName, epiNum)
            chase = False
            if 'Chase' in sceneName:
                chase = True
            self.load_network_agents(networkFilePath=os.path.join(self.agentPath, agentFileName), chase=chase)
            self.timers.append(Timer(startTime=self.elapsedTime, Time=self.episodeTime, callback=self.resetReplay, realTime=True))
        else:
            self.timers.append(Timer(Ticks=5, callback=self.endReplay))

    def resetReplay(self):
        print "Reseting for next replay"
        self.kill_agents()
        OpenNero.disable_ai()
        #self.write_agent_data()
        self.remove_agents()
        self.timers.append(Timer(startTime=self.elapsedTime, Time=self.restTime, callback=self.startReplayOrAgent, realTime=True))

    def agentFromScenario(self, sceneParameters=""):
        params = sceneParameters.split(',')
        sceneName = params[0]
        self.currScenario = sceneName
        epiNum = int(params[1].split(' ')[1])

        self.replayScenarioFiles = self.LoadUserData()
        self.practice = True

        self.resetSpeedUpSpeed = self.currSpeedUp
        #self.set_speedup(constants.DEFAULT_SPEEDUP)
        OpenNero.enable_ai()

        self.loadScenarioEnvironment(sceneName, epiNum)

        epiName = 'eps_{0}'.format(epiNum)
        agentFileName = '{0}_{1}.revccnet'.format(sceneName, epiName)
        trainingFile = next((x for x in self.replayScenarioFiles[sceneName]['training'] if epiName in os.path.basename(x)), None)
        chase = False
        if 'Chase' in sceneName:
            chase = True
        self.load_network_agents(trainingFilePath=trainingFile, saveNetToFile=agentFileName, chase=chase)
        #self.timers.append(Timer(startTime=self.elapsedTime, Time=self.episodeTime, callback=self.endScenario, realTime=True))
        self.timers.append(Timer(Time=self.episodeTime, callback=self.endReplay))

    def agentFromNetFile(self, sceneParameters):
        params = sceneParameters.split(',')
        sceneName = params[0]
        self.currScenario = sceneName
        epiNum = int(params[1].split(' ')[1])
        agentFileName = params[2]
        self.replayScenarioFiles = self.LoadUserData()
        self.practice = True
        self.resetSpeedUpSpeed = self.currSpeedUp
        #self.set_speedup(constants.DEFAULT_SPEEDUP)
        OpenNero.enable_ai()

        self.loadScenarioEnvironment(sceneName, epiNum)
        chase = False
        if 'Chase' in sceneName:
            chase = True
        self.load_network_agents(networkFilePath=os.path.join(self.agentPath, agentFileName), chase=chase)
        self.timers.append(Timer(startTime=self.elapsedTime, Time=self.episodeTime, callback=self.endReplay, realTime=True))
        #self.timers.append(Timer(Ticks=900, callback=self.endScenario))

    def testNetFromFile(self, sceneParameters=""):
        if sceneParameters != "":
            params = sceneParameters.split(',')
            sceneName = params[0]
            self.currScenario = sceneName
            self.epiNum = int(params[1])
            self.agentFileName = params[2]
            self.numTestTicks = int(params[3])
            self.netTestOutFile = params[4]
            self.netTestResults = []
        self.replayScenarioFiles = self.LoadUserData()
        self.practice = True
        self.resetSpeedUpSpeed = self.currSpeedUp
        #self.set_speedup(constants.DEFAULT_SPEEDUP)
        OpenNero.enable_ai()

        self.loadScenarioEnvironment(self.currScenario, self.epiNum)
        chase = False
        if 'Chase' in self.currScenario:
            chase = True
        self.load_network_agents(networkFilePath=os.path.join(self.agentPath, self.agentFileName), chase=chase)
        self.episodeCounter += 1
        print "Starting Episode: ", self.episodeCounter
        if self.episodeCounter >= self.numEpisodes:
            self.timers.append(Timer(Ticks=self.numTestTicks, callback=self.endNetTest))
        else:
            self.timers.append(Timer(Ticks=self.numTestTicks, callback=self.resetNetTest))

    def endNetTest(self):
        agent = self.environment.get_network_agent()
        rewards = agent.rewards
        self.netTestResults.append([x for x in rewards])
        with open(os.path.join(self.resultPath, self.netTestOutFile), 'w') as fileh:
            json.dump(self.netTestResults, fileh)

        del self.netTestResults[:]
        print "Ending Scenario: ", self.currScenario

        if "Shoot" in self.currScenario:
            constants.nextDisplayHint()

        self.kill_agents()
        OpenNero.disable_ai()
        self.set_speedup(self.resetSpeedUpSpeed)
        self.remove_agents()
        self.removeObstacles()
        self.episodeCounter = 0
        self.currScenario = ""
        self.clearTimers()
        self.clearScenarioPaths()
        self.scenarioEnvironmentLoaded = False
        OpenNero.getSimContext().killGame()

    def resetNetTest(self):
        agent = self.environment.get_network_agent()
        rewards = agent.rewards
        self.netTestResults.append([x for x in rewards])
        self.kill_agents()
        OpenNero.disable_ai()
        #self.write_agent_data()
        self.remove_agents()
        self.testNetFromFile()

    def loadScenarioEnvironment(self, sceneName, epiNum):
        #load the obstacles if they are there and the agents with the replay ai
        historyFiles = self.replayScenarioFiles[sceneName]['history']
        if self.scenarioEnvironmentLoaded is False:
            if 'Obstacles' in sceneName:
                print "Loading Saved Obstacles"
                obstacleFile = next((x for x in historyFiles if 'Obstacles' in os.path.basename(x)), None)
                self.addObstacles(filePath=obstacleFile)

            if 'Shoot' in sceneName:
                constants.nextDisplayHint()
                self.environment.allowShooting = True
            else:
                self.environment.allowShooting = False

            self.scenarioEnvironmentLoaded = True

        return historyFiles

    def loadReplayEnvironment(self, sceneName, epiNum):

        historyFiles = self.loadScenarioEnvironment(sceneName, epiNum)

        epiName = 'eps_{0}'.format(epiNum)
        episodeFile = next((x for x in historyFiles if epiName in x), None)

        print "Loading Agents from Episode {0}".format(epiNum)
        return self.load_replay_agents(filePath=episodeFile)

    def write_agent_data(self):
        # write out the history that can be replayed
        filename = "AgentHistory_eps_{0}.json".format(self.episodeCounter)
        filepath = os.path.join(self.historyPath, filename)
        fileh = open(filepath, "w")
        self.environment.write_agent_histories(fileh)
        fileh.close()

        # Write out the training data from the person
        filename2 = "PersonTrainingData_eps_{0}.json".format(self.episodeCounter)
        filepath2 = os.path.join(self.trainingPath, filename2)
        fileh = open(filepath2, "w")
        self.environment.write_agent_trainingData(fileh)
        fileh.close()

    def doSendEndMsg(self):
        """
        Called by the modules tick function
        Tells the modules whether or not to tell the gui that the scenario has ended
        """
        if self.sendEndMsg == True:
            self.sendEndMsg = False
            print "Sending End Message to GUI"
            return True
        else:
            return False

    def keepTime(self, elapsedTime):
        """Called by the modules tick function
        This is how we keep time it circulates through the timers to see
        if any of them need to be triggered

        Parameters
        ----------
        elapsedTime - The time in seconds since the beginning of the simulation

        Returns
        -------
        void
        """
        self.elapsedTime = elapsedTime
        for timer in self.timers[:]:  # The [:] is used so that I can remove from the list
            if timer.Ticks != None or timer.realTime:  # Using Ticks instead of time.
                if timer.Tick():
                    timer.callback()
                    if timer.repeat:
                        timer.ResetTick()
                    else:
                        if timer in self.timers:
                            self.timers.remove(timer)

            elif elapsedTime - timer.startTime > timer.Time:
                timer.callback()
                if timer.repeat:
                    timer.startTime = elapsedTime
                else:
                    if timer in self.timers:
                        self.timers.remove(timer)

    def testTimerCallback(self):
        print "I've been called back"

    def clearTimers(self):
        del self.timers[:]

    def getSpawnPoints(self):
        point1 = constants.getRandomSpawnPoint()
        point2 = constants.getRandomSpawnPoint()
        while point1 == point2:
            point2 = constants.getRandomSpawnPoint()

        return point1, point2

    def setUserInfo(self, name, path):
        if name != "None":
            self.userName = name
            self.userPath = path.strip("'")
            self.dataPath = os.path.join(self.userPath, "data")
            self.agentPath = os.path.join(self.userPath, "agents")
            self.resultPath = os.path.join(self.userPath, "results")
            if not os.path.exists(self.dataPath):
                os.mkdir(self.dataPath)
            if not os.path.exists(self.agentPath):
                os.mkdir(self.agentPath)
            if not os.path.exists(self.resultPath):
                os.mkdir(self.resultPath)
        # self.historyPath = os.path.join(self.userPath, "history")
        # self.dataPath = os.path.join(self.userPath, "data")
        # if self.verbose: print self.userPath
        # if self.verbose: print self.historyPath
        # if self.verbose: print self.dataPath
        # if not os.path.exists(self.historyPath):
        #     os.mkdir(self.historyPath)
        # if not os.path.exists(self.dataPath):
        #     os.mkdir(self.dataPath)

    def RandomReplayOrAgent(self, inParameters):
        params = inParameters.split(',')
        sceneName = params[0]
        episode = params[1]
        RAnum = int(params[2])

        epsNames = self.getSceneEpsNames(sceneName)
        sceneAgents = self.getSceneAgents(sceneName)
        consolAgents = [x for x in sceneAgents if 'Consol' in x]

        tuples = []
        for i in range(RAnum):
            if random() > 0.5:
                tuples.append(("Replay", "{0},{1}".format(sceneName, choice(epsNames))))
            else:
                if random() > 0.5:
                    tuples.append(("Agent", "{0},{1},{2}".format(sceneName, episode, choice(sceneAgents))))
                else:
                    tuples.append(("Agent", "{0},{1},{2}".format(sceneName, episode, choice(consolAgents))))

        self.ReplayList = (x for x in tuples)
        with open(os.path.join(self.resultPath, "{0}_replayorder.json".format(sceneName)), 'w') as fileh:
            json.dump(tuples, fileh)

        self.startReplayOrAgent()

    def spawnNetworkAgent(self, startPoint, trainingFilePath="", networkFilePath="", useFannedOut=True, saveNetToFile=""):
        network = None
        if trainingFilePath != "":
            network = self.createNetworkFromTraining(trainingFilePath, useFannedOut, saveNetToFile)
        elif networkFilePath != "":
            network = self.loadNetworkFromFile(networkFilePath)

        if network is not None:
            Id = common.addObject("data/character/steve_blue_network.xml", OpenNero.Vector3f(startPoint[0], startPoint[1], 0.2), type=constants.OBJECT_TYPE_TEAM_0)
            self.AgentIds.append(Id)
            self.AgentNetworks[Id] = network

    def createNetworkFromTraining(self, trainingFilePath, useFannedOut=True, saveNetToFile=""):
        if isinstance(trainingFilePath, str):
            trainingFilePath = [trainingFilePath]
        data = ObsUtility.LoadTrainingData(trainingFilePath)
        uSettings = self.LoadUserSettings(self.userName)
        numFrames = 900
        ds = self.CreateDataset(data, numFrames, normalize=True, splitOuts=True, settings=uSettings, useFannedTurn=useFannedOut)
        revCC = PyJRNN.trainers.RevCCTrainer(ds.numInputs, ds.numOutputs, 4)
        revCC.revparams.numRevTrainRatio = 1
        revCC.revparams.numRev = 2
        print "Training Network from training file"
        revCC.TrainTask(ds, 1000, True)
        print "Done Training"
        if saveNetToFile != "":
            self.saveNetworkToFile(saveNetToFile, revCC.net1)
        return revCC.net1

    def CreateDataset(self, data, numFrames, distNums=(), normalize=True, splitOuts=True, settings=None, useFannedTurn=False):
        retDS = PyJRNN.utility.Dataset()
        settings = settings or ObsUtility.UserSettings()
        numTrain = 0
        numTest = 0
        numVal = 0

        if len(distNums) > 0:
            numTrain = distNums[0]
            numVal = distNums[1]
            numTest = distNums[2]
        else:
            numTrain = 0.6 * numFrames
            numVal = 0.3 * numFrames
            numTest = 0.1 * numFrames

        minSize = min([len(x) for x in data])
        strideSize = minSize / numFrames

        inputs = numpy.array([item['sensors'] for item in data[0][-minSize::strideSize]])
        outputs = numpy.array([item['actions'] for item in data[0][-minSize::strideSize]])
        if splitOuts:
            outputs = ObsUtility.SplitAndAdjustObsOutputs(outputs, settings, useFannedTurn=useFannedTurn)
        if normalize:
            inputs = ObsUtility.Norm01Array(inputs)
            outputs = ObsUtility.Norm01Array(outputs)
        retDS.LoadFromMatDoubles(ObsUtility.matDoubleFromArray(inputs), ObsUtility.matDoubleFromArray(outputs), True)

        retDS.DistData(int(numTrain), int(numVal), int(numTest))

        return retDS

    def loadNetworkFromFile(self, filepath):
        archiver = PyJRNN.utility.JSONArchiver()
        return archiver.LoadFromFile(filepath)

    def saveNetworkToFile(self, filepath, network):
        totalfilePath = os.path.join(self.agentPath, filepath)
        archiver = PyJRNN.utility.JSONArchiver()
        archiver.SaveToFile(network, totalfilePath)
        print "Network Saved to File at: {0}".format(totalfilePath)


    def LoadUserSettings(self, user):
        #print __name__ 
        #print "Locals", 
        #print locals()
        #print "Globals"
        #for k, v in globals().items():
            #print k , "=", v
        sys.modules["__main__"].UserSettings = UserSettings
        filepath = os.path.join(self.userPath, "settings.pickle")
        uSettings = pickle.load(open(filepath))
        return uSettings    


gMod = None


def delMod():
    global gMod
    gMod = None


def getMod():
    global gMod
    if not gMod:
        gMod = JRNNExp1Module()
    return gMod


def ServerMain():
    print "Starting mod JRNNExp1"


def parseInput(strn):
    if len(strn) < 2:
        return

    # print strn.encode('ascii', 'ignore')
    mod = getMod()
    # loc, val = strn.split(' ', 1)
    # vali = 1

    if "|" in strn:
        tokens = strn.split("|")
        if tokens[0] == "Start":
            # if tokens[1] == "Chase&Obstacles" or tokens[1] == "Obstacles":
            #     mod.addObstacles()
            # mod.load_agents()
            mod.startScenario(tokens[1])
        if tokens[0] == "End":
            # if tokens[1] == "Chase&Obstacles" or tokens[1] == "Obstacles":
            #     mod.removeObstacles()
            # mod.remove_agents()
            # mod.clearTimers()
            mod.endScenario(False)
        if tokens[0] == "EndReplay":
            mod.endReplay(False)

        if tokens[0] == "Practice":
            mod.startScenario(tokens[1], practice=True)

        if tokens[0] == "Replay":
            # print 'Replay ' + tokens[1].encode('ascii', 'ignore')
            mod.replayScenario(tokens[1])

        if tokens[0] == "CreateRunAgent":
            mod.agentFromScenario(tokens[1])

        if tokens[0] == "RunAgent":
            mod.agentFromNetFile(tokens[1])

        if tokens[0] == "RandomReplayOrAgent":
            mod.RandomReplayOrAgent(tokens[1])

    if "+" in strn:
        tokens = strn.split("+")
        if tokens[0] == "JD":
            print "Joystick Deadzone: " + tokens[1]
            deadzone = int(tokens[1]) / 100.0
            # mod.io_map.SetJoystickDeadzone(deadzone)
            OpenNero.getSimContext().SetJoystickDeadzone(deadzone)
        elif tokens[0] == "TS":
            print "Turn Sensitivity: " + tokens[1]
            mod.turn_sensitivity = int(tokens[1])
        elif tokens[0] == "SP":
            print "Speed Up: " + tokens[1]
            mod.set_speedup(int(tokens[1]))
        elif tokens[0] == "EN":
            print "Number of Episodes: " + tokens[1]
            mod.numEpisodes = int(tokens[1])
        elif tokens[0] == "EL":
            print "Episode Length: " + tokens[1]
            mod.episodeTime = int(tokens[1])
        elif tokens[0] == "RL":
            print "Rest Length: " + tokens[1]
            mod.restTime = int(tokens[1])

    if "?" in strn:
        tokens = strn.split("?")
        # mod.userName = tokens[0]
        # mod.userPath = tokens[1]
        # print tokens
        mod.setUserInfo(tokens[0], tokens[1])


def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path:  # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts
