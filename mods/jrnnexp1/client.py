import math

import OpenNero
import common
import common.gui as gui
import constants
import module
import agent

guiMan = None


def switchToHub():
    module.delMod()
    OpenNero.switchMod('hub', 'hub:common')

def blank():
    pass

def createInputMapping():

    ioMap = agent.createInputMapping()

    ioMap.BindKey( "KEY_ESCAPE", "onPress", switchToHub)

    return ioMap

def ClientMain():
    global guiMan

    OpenNero.disable_ai()

    if not module.getMod().setup_map():
        switchToHub()
        return 

    # add a light source
    OpenNero.getSimContext().addLightSource(OpenNero.Vector3f(500, -500, 1000), 1500)

    common.addSkyBox("data/sky/irrlicht2")

    # setup the gui
    guiMan = common.getGuiManager()

    # add a camera
    camRotateSpeed = 100
    camMoveSpeed   = 15000
    camZoomSpeed   = 200
    cam = OpenNero.getSimContext().addCamera(camRotateSpeed, camMoveSpeed, camZoomSpeed)
    cam.setPosition(OpenNero.Vector3f(800, 800, 100))
    cam.setTarget(OpenNero.Vector3f(1, 1, 0))
    cam.setFarPlane(40000)
    cam.setEdgeScroll(False)

    def recenter(cam):
        def closure():
            # cam.setPosition(OpenNero.Vector3f(0, 0, 100))
            cam.setTarget(OpenNero.Vector3f(50, 0, 0))
        return closure

    recenter_cam = recenter(cam)
    recenter_cam()

    # create the io map
    ioMap = createInputMapping()
    # ioMap.BindKey("KEY_SPACE", "onPress", recenter_cam)
    OpenNero.getSimContext().setInputMapping(ioMap)
    module.getMod().io_map = ioMap
