# import constants
import cPickle
import socket
import struct
import sys
import os
import select
import time
import re
from threading import Thread
from UserSettings import UserSettings

try:
    import wx
except:
    import tkMessageBox
    tkMessageBox.showwarning('Warning!', 'Could not start the external menu because wxPython is not installed')
    sys.exit()

marshall = cPickle.dumps
unmarshall = cPickle.loads


#class UserSettings:
    #def __init__(self, turnSensitivity=30, deadzone=15, speedup=100):
        #self.turnSensitivity = turnSensitivity
        #self.deadzone = deadzone
        #self.speedup = speedup


class Slider:
    def __init__(self, label, key, span=200, center=100, thumb=100):
        self.label = label
        self.key = key
        self.span = span
        self.center = center
        self.thumb = thumb
        self.explanation = None
        self.scrollbar = None
        self.label = None
    def Enable(self):
        if self.label: self.label.Enable()
        if self.scrollbar: self.scrollbar.Enable()
        if self.explanation: self.explanation.Enable()
    def Disable(self):
        if self.label: self.label.Disable()
        if self.scrollbar: self.scrollbar.Disable()
        if self.explanation: self.explanation.Disable()


def send(channel, *args):
    buf = marshall(args)
    value = socket.htonl(len(buf))
    size = struct.pack("L", value)
    channel.send(size)
    channel.send(buf)


def receive(channel):
    size = struct.calcsize("L")
    size = channel.recv(size)
    try:
        size = socket.ntohl(struct.unpack("L", size)[0])
    except struct.error, e:
        return ''
    buf = ""
    while len(buf) < size:
        buf = channel.recv(size - len(buf))
    return unmarshall(buf)[0]


HOST = '127.0.0.1'
PORT = 8888

EVT_RESULT_ID = wx.NewId()


def EVT_RESULT(win, func):
    """Define Result Event."""
    win.Connect(-1, -1, EVT_RESULT_ID, func)


class ResultEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""
    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_RESULT_ID)
        self.data = data


class ReceiverThread(Thread):
    def __init__(self, notify_window):
        Thread.__init__(self)
        self._notify_window = notify_window
        self._want_abort = 0
        self._inputs = [self._notify_window.sock]
        self._outputs = []
        self.start()

    def run(self):
        print "Script Client Receiver Started"
        while self._want_abort == 0:
            data = self.read_data()
            if data:
                wx.PostEvent(self._notify_window, ResultEvent(data))
            time.sleep(1)

    def abort(self):
        self._want_abort = 1

    def read_data(self):
        try:
            inputready, outputready, exceptready = select.select(self._inputs, self._outputs, [], 0)
        except select.error, e:
            print 'ScriptClient select error'
            return None
        for s in inputready:
            try:
                data = receive(s)
                if data:
                    return data
                else:
                    print 'ScriptClient: %d hung up' % s.fileno()
                    s.close()
                    if s in self._inputs:
                        self._inputs.remove(s)
                    if s in self.outputs:
                        self.outputs.remove(s)
            except socket.error, e:
                print 'ScripClient socket error: %s' % e
                s.close()
                if s in self._inputs:
                    self._inputs.remove(s)
                if s in self.outputs:
                    self.outputs.remove(s)
        return None


class ScriptClient:
    def __init__(self, host=HOST, port=PORT):
        self.host = host
        self.port = port
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((self.host, self.port))
            print 'ScriptClient connected to ScriptServer at (%s, %d)' % (self.host, self.port)
        except socket.error, e:
            print 'ScriptClient could not connect to ScriptServer'

    def __del__(self):
        print 'ScriptClient closing socket...'
        self.sock.close()

    def send(self, data):
        if data and self.sock:
            try:
                send(self.sock, data)
            except:
                print data

    def receive(self):
        if self.sock:
            try:
                data = receive(self.sock)
            except:
                print "Error receiving data"
            return data


class JRNNExp1Panel(wx.Panel, ScriptClient):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        ScriptClient.__init__(self)
        self._masterRow = 0
        self._grid = wx.GridBagSizer(hgap=5, vgap=5)
        self._usersPath = None
        self._userPath = None
        self._userName = None
        self._scenarios = {}  # Used for Replays

        self.userSettings = UserSettings()
        self.userNameChoices = ["Select"]
        self.userNameChoices += self.check_for_users()
        self.userNameBox = self.add_dropdown("User", self.processUserBox, self.userNameChoices, True, self._masterRow)
        self._masterRow += 1
        self.scenarioChoices = ["Chase", "Obstacles", "ChaseObstacles", "ChaseShoot", "ChaseObstaclesShoot"]
        self.scenarioBox = self.add_dropdown("Scenario", self.processScenarioBox, self.scenarioChoices, False, self._masterRow)
        self._masterRow += 1

        self._buttonIndex = 0
        self._buttonPanel = wx.Panel(self, wx.ID_ANY)
        self._buttonGrid = wx.GridBagSizer(hgap=5, vgap=5)
        self._buttons = {}
        self.add_buttons()
        self._buttonPanel.SetSizer(self._buttonGrid)

        self._grid.Add(self._buttonPanel, pos=(self._masterRow, 0), span=(1, 6), border=5, flag=wx.LEFT|wx.EXPAND)
        self._masterRow += 1

        self._sliderIndex = 0
        self._sliderPanel = wx.Panel(self, wx.ID_ANY)
        self._sliderGrid = wx.GridBagSizer(hgap=5, vgap=5)
        self._sliders = {}
        self.add_sliders()
        self._sliderPanel.SetSizer(self._sliderGrid)
        self._grid.Add(self._sliderPanel, pos=(self._masterRow, 0), span=(1, 6), border=5, flag=wx.LEFT|wx.EXPAND)
        self._masterRow += 1
        self.SendSliders()

        # Replay Controls
        self._replayPanel = wx.Panel(self, wx.ID_ANY)
        self._replayGrid = wx.GridBagSizer(hgap=5, vgap=5)
        self._replayBox = wx.StaticBoxSizer(wx.StaticBox(self._replayPanel, wx.ID_ANY, 'ReplayBox', style=wx.RESIZE_BOX), wx.HORIZONTAL)
        self._replayControls = {}
        self._replayEnabled = False  # Start as true so that the first toggle disables them. 
        #self._scenario
        self._scenarioNames = ['Scenario']  # Used for Replays
        self._episodeNames = ['Episode']  # Used for Replays
        self._agentNames = ['Agents']  # Used for Replays
        self.add_replay_controls()
        self._replayPanel.SetSizer(self._replayGrid)
        self._grid.Add(self._replayPanel, pos=(self._masterRow, 0), span=(1, 6), border=5, flag=wx.LEFT|wx.EXPAND)
        self._masterRow += 1


        self.SetSizer(self._grid)
        self.loaded1 = False

        self._grid.Fit(parent)

        EVT_RESULT(self, self.OnReceive)  # Bind the thread result to a function.
        self.receiver = ReceiverThread(self)  # Start a thread that listens for a response

    def __del__(self):
        print "Stopping Client Receiver Thread"
        self.receiver.abort()

    def check_for_users(self):
        users_path = os.path.join(os.curdir, "jrnnexp1", "users")
        self._usersPath = users_path
        users = [d for d in os.listdir(users_path) if os.path.isdir(os.path.join(users_path, d))]
        # print os.path.abspath(users_path)
        # print users
        return users

    def get_user_scenarios(self):
        self._scenarios = self.LoadUserData(self._userName)
        self._scenarioNames = sorted(self._scenarios.keys())

    def loadScenarioFiles(self, scenarioPath):
        scenariofiles = {}
        trainingdir = os.path.join(scenarioPath, "training")
        historydir = os.path.join(scenarioPath, "history")
        scenariofiles["training"] = [os.path.join(trainingdir, x) for x in os.listdir(trainingdir)]
        scenariofiles["history"] = [os.path.join(historydir, x) for x in os.listdir(historydir)]
        return scenariofiles

    def LoadUserData(self, username):
        # global userspath
        Scenarios = {}
        userP = os.path.join(self._usersPath, username)
        userDataP = os.path.join(userP, "data")
        scenarioList = os.listdir(userDataP)
        for scene in scenarioList:
            Scenarios[scene] = self.loadScenarioFiles(os.path.join(userDataP, scene))

        return Scenarios

    def LoadUserAgents(self, username):
        agentlist = []
        userAgentP = os.path.join(self._userPath, "agents")
        agentlist = os.listdir(userAgentP)
        return agentlist

    def add_replay_controls(self):
        self.add_button('Load Replay', self.loadReplayControls, sizer=self._replayGrid, panel=self._replayPanel, key='LoadReplays', pos=(0, 0))
        self.add_button('End Replay', self.sendEndReplay, sizer=self._replayGrid, panel=self._replayPanel, key='EndReplay', pos=(0, 1))
        replayBoxGrid = wx.GridBagSizer(5, 5)
        replayBoxGrid.SetFlexibleDirection(wx.BOTH)
        replayBoxGrid.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self._replayControls['scenario'] = self.add_dropdown('Scenario', self.loadReplayScen, self._scenarioNames, False, 0, replayBoxGrid, self._replayPanel)
        self._replayControls['episode'] = self.add_dropdown('Episode', self.loadReplayEps, self._episodeNames, False, 1, replayBoxGrid, self._replayPanel)
        self.add_button('Run Replay', self.RunReplay, sizer=replayBoxGrid, panel=self._replayPanel, key='RunReplay', pos=(2, 0))
        self.add_button('Create and Run Agent', self.CreateRunAgent, sizer=replayBoxGrid, panel=self._replayPanel, key='CreateRunAgent', pos=(2, 1))
        self._replayControls['agents'] = self.add_dropdown('Agents', self.loadReplayAgents, self._agentNames, False, 3, replayBoxGrid, self._replayPanel)
        self.add_button('Run Agent', self.RunAgent, sizer=replayBoxGrid, panel=self._replayPanel, key='RunAgent', pos=(4, 0))
        self._replayControls['RA Number'] = self.add_spinCtrl('RA Number', replayBoxGrid, self._replayPanel, False, pos=(5, 0), inmin=0, inmax=30, initial=20)
        self.add_button('Run Replays and Agents', self.RunReplaysAndAgents, sizer=replayBoxGrid, panel=self._replayPanel, key='RunReplaysAndAgents', pos=(6, 0))
        self._replayControls['runreplaysandagents'] = self._buttons['RunReplaysAndAgents']
        self._replayControls['runagent'] = self._buttons['RunAgent']
        self._replayBox.Add(replayBoxGrid, 1, wx.EXPAND, 5)
        self._replayControls['runreplay'] = self._buttons['RunReplay']
        self._replayControls['createrunagent'] = self._buttons['CreateRunAgent']
        self._replayGrid.Add(self._replayBox, pos=(1, 0), span=(1, 2), flag=wx.EXPAND, border=5)
        self.disableReplayControls()

    def loadReplayControls(self, event):
        self.get_user_scenarios()
        self._replayControls['scenario'].SetItems(self._scenarioNames)
        self._agentNames = self.LoadUserAgents(self._userName)
        self._replayControls['agents'].SetItems(self._agentNames)
        self._replayGrid.Layout()  # this is still not doing what I want it too. Need to figure out how to resize the static box.
        self._replayGrid.Fit(self._replayPanel)
        self._replayEnabled = True
        self.enableReplayControls()

    def resetReplayControls(self):
        self._replayControls['scenario'].SetItems(['Scenarios'])
        self._replayControls['episode'].SetItems(['Episodes'])
        self._replayControls['agents'].SetItems(['Agents'])
        self._scenarios.clear()
        del self._scenarioNames[:]
        del self._episodeNames[:]
        self._replayEnabled = False

    def loadReplayScen(self, event):
        ScenName = self._replayControls['scenario'].GetValue()
        epsNum = re.compile('eps_(\d+)')
        filePaths = self._scenarios[ScenName]['history']
        fileNames = sorted([os.path.basename(x) for x in filePaths if 'eps' in x], key=lambda x: int(epsNum.search(x).group(1)))
        epsNames = ['Episode {0}'.format(epsNum.search(x).group(1)) for x in fileNames]
        self._replayControls['episode'].SetItems(epsNames)

    def loadReplayEps(self, event):
        pass

    def loadReplayAgents(self, event):
        pass

    def RunReplay(self, event):
        replaystring = ""
        sceneName = self._replayControls['scenario'].GetValue()
        epsName = self._replayControls['episode'].GetValue()
        replaystring = "{0},{1}".format(sceneName, epsName)
        self.send('Replay|{0}'.format(replaystring))
        self.disableUserSceneSelect()
        self.disableReplayButtons()

    def RunReplaysAndAgents(self, event):
        raNum = self._replayControls['RA Number'].GetValue()
        sceneName = self._replayControls['scenario'].GetValue()
        epsName = self._replayControls['episode'].GetValue()
        self.send('RandomReplayOrAgent|{0},{1},{2}'.format(sceneName, epsName, raNum))
        self.disableUserSceneSelect()
        self.disableReplayControls()

    def CreateRunAgent(self, event):
        replaystring = ""
        sceneName = self._replayControls['scenario'].GetValue()
        epsName = self._replayControls['episode'].GetValue()
        replaystring = "{0},{1}".format(sceneName, epsName)
        self.send('CreateRunAgent|{0}'.format(replaystring))
        self.disableUserSceneSelect()
        self.disableReplayButtons()

    def RunAgent(self, event):
        agentFileName = self._replayControls['agents'].GetValue()
        sceneName = self._replayControls['scenario'].GetValue()
        epsName = self._replayControls['episode'].GetValue()
        self.send('RunAgent|{0},{1},{2}'.format(sceneName, epsName, agentFileName))
        self.disableUserSceneSelect()
        self.disableReplayControls()

    def enableReplayControls(self):
        for key, val in self._replayControls.iteritems():
                val.Enable()

    def disableReplayControls(self, reset=False):
        for key, val in self._replayControls.iteritems():
                val.Disable()
        if reset:
            self.resetReplayControls()

    def add_user_dropdown(self):
        dd_label = wx.StaticText(self, wx.ID_ANY, u"User", wx.DefaultPosition, wx.DefaultSize, 0)
        self._grid.Add(dd_label, pos=(0, 0), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)
        user_dd = wx.ComboBox(self, wx.ID_ANY, u"Select", choices=self.userNameChoices, style=wx.CB_DROPDOWN | wx.TE_PROCESS_ENTER)
        self._grid.Add(user_dd, pos=(0, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)
        user_dd.Bind(wx.EVT_COMBOBOX, self.processUserBox)
        user_dd.Bind(wx.EVT_TEXT_ENTER, self.processUserBox)
        return user_dd

    def add_dropdown(self, label, callback, ddchoices, editable=False, rowIndex=0, sizer=None, panel=None):
        sizer = self._grid if sizer == None else sizer
        panel = self if panel == None else panel
        dd_label = wx.StaticText(panel, wx.ID_ANY, label, wx.DefaultPosition, wx.DefaultSize, 0)
        ddstyle = wx.CB_DROPDOWN | wx.TE_PROCESS_ENTER if editable else wx.CB_READONLY
        sizer.Add(dd_label, pos=(rowIndex, 0), flag=wx.LEFT | wx.ALIGN_CENTER_VERTICAL, border=5)
        dropdown = wx.ComboBox(panel, wx.ID_ANY, ddchoices[0], choices=ddchoices, style=ddstyle)
        sizer.Add(dropdown, pos=(rowIndex, 1), flag=wx.LEFT | wx.ALIGN_CENTER_VERTICAL, border=5)
        dropdown.Bind(wx.EVT_COMBOBOX, callback)
        if editable:
            dropdown.Bind(wx.EVT_TEXT_ENTER, callback)
        return dropdown

    def add_buttons(self):
        # self.add_button("Test", self.TestButton)
        # self.add_button("Start", self.StartScenario)
        # self.add_button("End", self.EndScenario)
        self.add_control_buttons("Scenario Controls", "controls")

    def add_button(self, label, callback, sizer=None, panel=None, disabled=False, key=None, pos=None, span=None):
        buttonPanel = self._buttonPanel if panel == None else panel
        button = wx.Button(
            buttonPanel, pos=wx.DefaultPosition, size=wx.DefaultSize, label=label)
        if sizer == None:
            self._buttonGrid.Add(button, pos=(self._buttonIndex / 3, self._buttonIndex % 3))
            self._buttonIndex += 1
        elif pos != None:
            span = span if span != None else (1, 1)
            sizer.Add(button, pos=pos, span=span)
        else:
            sizer.Add(button, 0, wx.ALL, 5)

        self.Bind(wx.EVT_BUTTON, callback, button)
        key = callback.__name__ if key == None else key
        self._buttons[key] = button
        if disabled:
            button.disabled

    def add_spinCtrl(self, label, sizer, panel, disabled=False, pos=None, span=None, inmin=0, inmax=40, initial=20):
        spinPanel = panel
        spinC = wx.SpinCtrl(spinPanel, pos=wx.DefaultPosition, size=wx.DefaultSize, value=wx.EmptyString, style=wx.SP_ARROW_KEYS, min=inmin, max=inmax, initial=initial)
        sc_label = wx.StaticText(panel, wx.ID_ANY, label, wx.DefaultPosition, wx.DefaultSize, 0)
        sizer.Add(sc_label, pos=pos, flag=wx.LEFT | wx.ALIGN_CENTER_VERTICAL, border=5)
        if pos != None:
            span = span if span != None else (1, 1)
            sizer.Add(spinC, pos=(pos[0], pos[1]+1), span=span)
        else:
            sizer.Add(spinC, 0, wx.ALL, 5)

        if disabled:
            spinC.disabled

        return spinC

    def add_control_buttons(self, name, cbprefix=None, parentPanel=None, parentSizer=None):
        #set defaults if they are None
        parentPanel = self._buttonPanel if parentPanel == None else parentPanel
        parentSizer = self._buttonGrid if parentSizer == None else parentSizer
        cbprefix = name if cbprefix == None else cbprefix
        #create the static box sizer
        scenarioBox = wx.StaticBoxSizer(wx.StaticBox(parentPanel, wx.ID_ANY, name), wx.HORIZONTAL)
        #add the scenario buttons
        self.add_button("Practice", lambda event: self.OnPractice(event, cbprefix), scenarioBox, key="OnPractice")
        self.add_button("Start", lambda event: self.OnStart(event, cbprefix), scenarioBox, key="OnStart")
        self.add_button("End", lambda event: self.OnEnd(event, cbprefix), scenarioBox, key="OnEnd")
        #add the scenario box to the parent sizer
        parentSizer.Add(scenarioBox, pos=(self._buttonIndex / 3, self._buttonIndex % 3),  flag=wx.EXPAND, border=5)
        self.disableControlButtons()
        if parentSizer == self._buttonGrid:
            self._buttonIndex += 1

    def enableControlButtons(self):
        self._buttons["OnPractice"].Enable()
        self._buttons["OnStart"].Enable()
        self._buttons["OnEnd"].Enable()

    def disableControlButtons(self):
        self._buttons["OnPractice"].Disable()
        self._buttons["OnStart"].Disable()
        self._buttons["OnEnd"].Disable()

    def disableReplayButtons(self, reset=False):
        self._buttons["LoadReplays"].Disable()
        self.disableReplayControls(reset)

    def enableReplayButtons(self):
        self._buttons["LoadReplays"].Enable()

    def add_sliders(self):
        joystickSliders = wx.StaticBoxSizer(wx.StaticBox(self._sliderPanel, wx.ID_ANY, "Joystick Parameters"), wx.VERTICAL)
        joystickGrid = wx.GridBagSizer(5, 5)
        joystickGrid.SetFlexibleDirection(wx.BOTH)
        joystickGrid.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self.add_slider("Deadzone", "JD", span=50, center=0, thumb=self.userSettings.deadzone, sizer=joystickGrid, index=0)
        self.add_slider("Turn Sensitivity", "TS", span=100, center=0, thumb=self.userSettings.turnSensitivity, sizer=joystickGrid, index=1)
        self.add_slider("Simulation Speed", "SP", span=100, center=0, thumb=self.userSettings.speedup, sizer=joystickGrid, index=2)
        joystickGrid.AddGrowableCol(1)
        joystickSliders.Add(joystickGrid, 1, wx.EXPAND, 5)
        self._sliderGrid.Add(joystickSliders, pos=(self._sliderIndex, 0))
        self._sliderIndex += 1
        # Parameter Sliders
        parameterSliders = wx.StaticBoxSizer(wx.StaticBox(self._sliderPanel, wx.ID_ANY, "Parameters"), wx.VERTICAL)
        parameterGrid = wx.GridBagSizer(5, 5)
        parameterGrid.SetFlexibleDirection(wx.BOTH)
        parameterGrid.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)
        self.add_slider("Number of Episodes", "EN", span=20, center=0, thumb=10, sizer=parameterGrid, index=0)
        self.add_slider("Episode Length", "EL", span=60, center=0, thumb=30, sizer=parameterGrid, index=1)
        self.add_slider("Rest Length", "RL", span=30, center=0, thumb=5, sizer=parameterGrid, index=2)
        parameterGrid.AddGrowableCol(1)
        parameterSliders.Add(parameterGrid, 1, wx.EXPAND, 5)
        self._sliderGrid.Add(parameterSliders, pos=(self._sliderIndex, 0))
        self._sliderIndex += 1
        self.disableSliders()

    def add_slider(self, label, key, span=200, center=100, thumb=100, parent=None, sizer=None, index=-1):
        slider = Slider(label, key, span, center, thumb)
        sizer = self._grid if sizer == None else sizer
        parent = self._sliderPanel if parent == None else parent
        slider.explanation = wx.StaticText(
            parent, label=label, pos=wx.DefaultPosition, size=wx.DefaultSize)

        slider.scrollbar = wx.ScrollBar(
            parent, pos=wx.DefaultPosition, size=(200, 15))
        slider.scrollbar.SetScrollbar(wx.HORIZONTAL, 0, span, span)
        slider.scrollbar.SetThumbPosition(thumb)

        pos = slider.scrollbar.GetThumbPosition() - center
        slider.label = wx.StaticText(
            parent, label=str(pos), pos=wx.DefaultPosition, size=wx.DefaultSize)

        self.Bind(wx.EVT_SCROLL_CHANGED, lambda event: self.OnSlider(event, key, center), slider.scrollbar)
        if sys.platform != 'win32':
            print 'not windows'
            self.Bind(wx.EVT_SCROLL_THUMBRELEASE, lambda event: self.OnSlider(event, key, center), slider.scrollbar)
        self.Bind(wx.EVT_SCROLL_THUMBTRACK, lambda event: self.OnTrackSlider(event, key, center), slider.scrollbar)
        curIndex = index if index != -1 else self._sliderIndex
        sizer.Add(slider.label, pos=(curIndex, 1))
        sizer.Add(slider.scrollbar, pos=(curIndex, 2))
        sizer.Add(slider.explanation, pos=(curIndex, 0))

        if index == -1:
            self._sliderIndex += 1

        self._sliders[key] = slider

    def disableSliders(self):
        for key, slider in self._sliders.items():
            slider.Disable()

    def enableSliders(self):
        for key, slider in self._sliders.items():
            slider.Enable()

    def SendSliders(self):
        for key, slider in self._sliders.items():
            self.send("%s+%d" % (key, slider.scrollbar.GetThumbPosition()))

    def SaveUserSettings(self):
        filename = os.path.join(self._userPath, "settings.pickle")
        outfile = open(filename, "w")
        cPickle.dump(self.userSettings, outfile)

    def GetUserSettings(self):
        filename = os.path.join(self._userPath, "settings.pickle")
        if not os.path.exists(filename) or not os.path.isfile(filename):
            self.SaveUserSettings()
        infile = open(filename, "r")
        self.userSettings = cPickle.load(infile)

    def updateSliders(self):
        self._sliders["JD"].scrollbar.SetThumbPosition(self.userSettings.deadzone)
        self._sliders["JD"].label.SetLabel(str(self.userSettings.deadzone))
        self._sliders["TS"].scrollbar.SetThumbPosition(self.userSettings.turnSensitivity)
        self._sliders["TS"].label.SetLabel(str(self.userSettings.turnSensitivity))
        self._sliders["SP"].scrollbar.SetThumbPosition(self.userSettings.speedup)
        self._sliders["SP"].label.SetLabel(str(self.userSettings.speedup))
        self.SendSliders()

    def enableUserSceneSelect(self):
        self.userNameBox.Enable()
        self.scenarioBox.Enable()

    def disableUserSceneSelect(self):
        self.userNameBox.Disable()
        self.scenarioBox.Disable()

    #Call Back functions
    def TestButton(self, event):
        print "This is a test Button"

    def StartScenario(self, event):
        self.send("Start")

    def EndScenario(self, event):
        self.send("End")

    def sendEndReplay(self, event):
        self.send("EndReplay|controls")
        self.enableUserSceneSelect()
        if self._replayEnabled:
            self.enableReplayControls()
            self.enableReplayButtons()

    def OnSlider(self, event, key, center=0):
        position = event.Position - center
        self._sliders[key].label.SetLabel(str(position))
        self.send("%s+%d" % (key, event.Position))
        if key == "JD":
            self.userSettings.deadzone = event.Position
        elif key == "TS":
            self.userSettings.turnSensitivity = event.Position
        elif key == "SP":
            self.userSettings.speedup = event.Position
        self.SaveUserSettings()

    def OnTrackSlider(self, event, key, center=0):
        position = event.Position - center
        self._sliders[key].label.SetLabel(str(position))

    def OnPractice(self, event, scenario):
        if scenario == "controls":
            sceneType = self.scenarioBox.GetValue()
            self.send("Practice|" + sceneType)
            self.disableUserSceneSelect()

    def OnStart(self, event, scenario):
        if scenario == "controls":
            sceneType = self.scenarioBox.GetValue()
            self.send("Start|" + sceneType)
            self.disableUserSceneSelect()

    def OnEnd(self, event, scenario):
        if scenario == "controls":
            sceneType = self.scenarioBox.GetValue()
            self.send("End|" + sceneType)
            self.enableUserSceneSelect()
            if self._replayEnabled:
                self.enableReplayControls()
                self.enableReplayButtons()


    def OnReceive(self, event):
        data = event.data
        print "Received Data from the ScriptServer: ", data
        if "End" in data:
            self.enableUserSceneSelect()
            if self._replayEnabled:
                self.enableReplayControls()
                self.enableReplayButtons()

    def processUserBox(self, event):
        print "User Name: ", self.userNameBox.GetValue()
        setName = self.userNameBox.GetValue()
        userPath = None
        self.disableReplayButtons(True)
        if setName == "Select":
            self.disableControlButtons()
            self.disableSliders()
            self._userName = None
            self._userPath = None
            self.send("None?'None'")
            return
        if setName not in self.userNameChoices:
            self.userNameChoices.append(setName)
            self.userNameBox.Append(setName)
            userPath = os.path.join(self._usersPath, setName)
            os.mkdir(userPath)
            self._userPath = userPath
            self.SaveUserSettings()
        else:
            userPath = os.path.join(self._usersPath, setName)
            self._userPath = userPath
            self.GetUserSettings()
            self.updateSliders()
        self._userName = setName
        self.send("{0}?'{1}'".format(self._userName, os.path.abspath(self._userPath)))
        self.enableControlButtons()
        self.enableReplayButtons()
        self.enableSliders()

    def processScenarioBox(self, event):
        pass

if __name__ == '__main__':
    app = wx.App(False)
    frame = wx.Frame(None, title="JRNNExp1 Controls", size=(600, 250))
    panel = JRNNExp1Panel(frame)
    frame.Show()
    app.MainLoop()
