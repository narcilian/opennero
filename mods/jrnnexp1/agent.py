from OpenNero import *  # import the OpenNERO C++ API
from random import random as rand
import module
import constants
import math
import json
import itertools
import numpy
from collections import deque
# import PyJRNN
# import pyublas
from PID import PID


class JRNNAgent(object):
    """
    base class for JRNN Agents
    """
    def __init__(self, team=None, group='Agent'):
        self.team = team or module.getMod().curr_team
        self.group = group
        self.info = AgentInitInfo(*self.agent_info_tuple())
        self.history = []
        self.dead = False

    def get_team(self):
        return self.team

    def agent_info_tuple(self):
        abound = FeatureVectorInfo()  # actions
        sbound = FeatureVectorInfo()  # sensors

        # actions
        abound.add_continuous(-1, 1)  # forward/backward speed
        abound.add_continuous(-1, 1)  # left/right turn (in radians)
        abound.add_discrete(0, 1)  # Fire or don't fire

        # sensor dimensions
        for a in range(constants.N_SENSORS):
            sbound.add_continuous(0, 1)

        return sbound, abound, module.jrnnexp1_rewards()

    def initialize(self, init_info):
        self.actions = init_info.actions
        self.sensors = init_info.sensors
        return True

    def end(self, time, reward):
        return True

    def writeHistory(self, toWrite):
        # tmpStr = json.dumps(toWrite)
        self.history.append(toWrite)

    def getHistory(self):
        return self.history

    def clearHistory(self):
        del self.history[:]

    def destroy(self):
        env = get_environment()
        if env is not None:
            env.remove_agent(self)
        return True


class NetworkAgent(JRNNAgent, AgentBrain):
    """
    An Agent that uses a JRNN Network to make decisions
    """

    ai = "Network"

    def __init__(self):
        AgentBrain.__init__(self)
        JRNNAgent.__init__(self)

        self.mNetwork = None
        self.mNetworkFilePath = None
        self.useFannedOut = True
        self.useShooting = False
        self.rewards = []
        self.numContexts = 0

    def initialize(self, init_info):
        self.actions = init_info.actions
        self.sensors = init_info.sensors
        return True

    def adjustinputs(self, sensors):
        obstacles = sensors[0:7]
        enemies = sensors[7:19]
        enemydistang = sensors[19:21]
        enemies[(enemies > 0) & (enemies < 0.6)] = 0.6
        #enemies[(enemies > 0)] = 1.0
        newinput = None
        if numContexts == 0:
            newinput = numpy.concatenate((obstacles, enemies, enemydistang))
        else:
            context = numpy.zeros(self.numContexts)
            context[-1] = 1
            newinput = numpy.concatenate((obstacles, enemies, enemydistang, context))
        return newinput

    def start(self, time, sensors):
        self.mNetwork = module.getMod().AgentNetworks[self.state.id]
        self.numContexts = self.mNetwork.numIn - len(sensors)
        inputs = numpy.array([x for x in sensors])
        output = self.Activate(self.adjustinputs(inputs))
        actions = self.actions.get_instance()
        if len(output) > 5:
            self.useFannedOut = True
        else:
            self.useFannedOut = False
            
        if len(output) == 5 or len(output) == 10:
            self.useShooting = True
        else:
            self.useShooting = False
        
        #for i, v in enumerate(output):
            #actions[i] = v
        self.mapOutputToActions(output, actions)
        return actions

    def act(self, time, sensors, reward):
        inputs = numpy.array([x for x in sensors])
        output = self.Activate(self.adjustinputs(inputs))
        actions = self.actions.get_instance()
        #for i, v in enumerate(output):
            #actions[i] = v
        self.mapOutputToActions(output, actions)
        # print "outputs: ", output, "actions: ", actions
        #print "actions: ", actions
        #print "Reward {0}".format(reward[0])
        self.rewards.append([x for x in reward])
        return actions

    def mapOutputToActions(self, output, actions):
        a1 = output[0:2]
        a2 = a3 = None
        if self.useFannedOut:
            a2 = output[2:9]
            if self.useShooting:
                a3 = 1 if output[9] > 0.6 else 0
        else:
            a2 = output[2:4]
            if self.useShooting:
                a3 = 1 if output[4] > 0.6 else 0
        
        actions[0] = self.twoToOne(a1)
        actions[1] = self.fanToOne(a2) if self.useFannedOut else self.twoToOne(a2)
        # ts = module.getMod().turn_sensitivity / 100.0
        # ts = 0.01 if ts == 0.0 else ts
        ts = 1
        actions[1] = actions[1] * ts
        if self.useShooting:
            actions[2] = a3
        #actions[0] = 1
        #actions[1] = 0
        
    def twoToOne(self, twoList):
        a = 0
        index = twoList.argmax()
        if index == 0:
            a = -twoList[0]
        else:
            a = twoList[1]
        return a
    
    def fanToOne(self, fanList):
        # This needs to match what happens to map the normal actions out to to network. 
        a = 0
        b = [-1, -0.5, -0.2, 0, 0.2, 0.5, 1]
        index = fanList.argmax()# fanList.index(max(fanList))# [i for i, v in enumerate(fanList) if v > 0]
        a = b[index]
        return a
    
    def Activate(self, inputs):
        self.mNetwork.Activate(inputs)
        output = None
        if self.mNetwork.type == "RevCC" or self.mNetwork.type == "RevKBCC":
            output = self.mNetwork.GetTrueOutputs()
        else:
            output = self.mNetwork.GetOutputs()
        return output

    def end(self, time, reward):
        self.rewards.append([x for x in reward])
        print "Network Agent Reward: {0}".format(reward)

    def destroy(self):
        del self.rewards[:]
        pass

    # def loadNetworkFromFile(self, filepath):
    #     pass

    # def matDoubleFromArray(inMat):
    #     newMat = PyJRNN.types.matDouble()
    #     for vec in inMat:
    #         if isinstance(vec, numpy.float64):
    #             newMat.append(numpy.array([vec]))
    #         else:
    #             newMat.append(vec)
    #     return newMat

class ReplayAgent(JRNNAgent, AgentBrain):
    """
    An Agent that replays a history file.
    """

    ai = "Replay"

    def __init__(self):
        AgentBrain.__init__(self)
        JRNNAgent.__init__(self)
        self.history = []
        self.filepath = ""
        self.initial_position = Vector3f(0, 0, 0)
        self.initial_rotation = Vector3f(0, 0, 0)
        self.currentstep = {}
        self.historycycle = None
        self.historyLoaded = False
        self.historyLength = 0
        self.historyCount = 0
        self.agentName = ""

    def initialize(self, init_info):
        self.actions = init_info.actions
        self.sensors = init_info.sensors
        return True

    def start(self, time, sensors):
        if self.historyLoaded and self.historyCount < self.historyLength:
            self.currentstep = self.historycycle.next()
            self.historyCount += 1
        actions = self.actions.get_instance()
        return actions

    def act(self, time, sensors, reward):
        if self.historyLoaded and self.historyCount < self.historyLength:
            self.currentstep = self.historycycle.next()
            self.historyCount += 1
        else:
            self.dead = True
        actions = self.actions.get_instance()
        return actions

    def end(self, time, reward):
        pass

    def destroy(self):
        pass

    def LoadHistoryFile(self, stringpath, agentname):
        with open(stringpath, 'r') as fileh:
            self.agentName = agentname
            historydict = json.load(fileh)
            temphist = historydict[agentname]
            startpos = temphist[0]
            self.history = temphist[1:]
            self.initial_position = Vector3f(startpos['initial_position']['x'], startpos['initial_position']['y'], startpos['initial_position']['z'])
            self.initial_rotation = Vector3f(startpos['initial_rotation']['x'], startpos['initial_rotation']['y'], startpos['initial_rotation']['z'])
            self.historycycle = itertools.cycle(self.history)
            self.currentstep = self.historycycle.next()
            self.historyLoaded = True
            self.historyLength = len(self.history)
            return True

    def GetCurrentStep(self):
        return self.currentstep

    def set_display_hint(self):
        """
        Set the display hint above the agents head.
        """
        display_hint = constants.getDisplayHint()
        if display_hint and self.agentName == 'Runner':
            if display_hint == 'damage':
                self.state.label = "{0}".format(get_environment().get_damage(self))
            else:
                self.state.label = '?'
        else:
            if self.state.label:
                self.state.label = ""


class PersonAgent(JRNNAgent, AgentBrain):
    """
    A simple agent with keyboard controls to move forward, back, left, and right.
    """

    ai = "Person"
    keys = set([])  # keys pressed
    action_map = {'FWD': 0, 'BCK': 1, 'LFT': 2, 'RHT': 3, 'FRE': 4}
    y_cursor = 0
    x_cursor = 0
    z_cursor = 0
    use_joystick = False

    def __init__(self):
        """
        This is the constructor - it gets called when the brain object is first created
        """
        # call the parent constructor
        AgentBrain.__init__(self)  # do not remove!
        JRNNAgent.__init__(self)
        self.trainingData = []

    def initialize(self, init_info):
        """
        Called when the agent is first created
        """
        self.actions = init_info.actions
        self.sensors = init_info.sensors
        return True

    def start(self, time, sensors):
        """
        Take the initial sensors and return the first action
        """
        # Ignore sensors and just return the action.

        actions = self.key_action()
        # print "sensors: ", self.sensors.normalize(sensors), "actions: ", actions
        self.clearTrainingData()
        self.clearHistory()
        self.trainingData.append(self.convertTData(sensors, actions))
        return actions

    def act(self, time, sensors, reward):
        """
        Take new sensors and reward from previous action and return the next action
        """
        # Ignore sensors and reward and just return the action.
        # print sensors

        actions = self.key_action()
        # print "sensors: ", self.sensors.normalize(sensors), "actions: ", actions
        self.trainingData.append(self.convertTData(sensors, actions))

        return actions

    def end(self, time, reward):
        """
        Take the last reward
        """
        # Ignore the reward here as well.
        return True

    def destroy(self):
        """
        Called when the agent is destroyed
        """
        return True

    def getTrainingData(self):
        return self.trainingData

    def clearTrainingData(self):
        del self.trainingData[:]

    def convertTData(self, sensors, actions):
        sensorarray = [x for x in sensors]
        actionsarray = [x for x in actions]
        return {"sensors": sensorarray, "actions": actionsarray}

    def key_action(self):
        actions = self.actions.get_instance()
        #assert(len(actions) == 2)

        # Keys specify movement action relative to screen coordinates.
        actions[0] = 0
        actions[1] = 0
        actions[2] = 0
        ts = module.getMod().turn_sensitivity / 100.0
        ts = 0.01 if ts == 0.0 else ts

        if len(PersonAgent.keys) > 0:
            for key in PersonAgent.keys:
                if key == 'RHT':
                    actions[1] = -1 * ts
                elif key == 'LFT':
                    actions[1] = 1 * ts
                elif key == 'FWD':
                    actions[0] = 1
                elif key == 'BCK':
                    actions[0] = -1
                elif key == 'FRE':
                    actions[2] = 1

            # print "keys: ", ' '.join(PersonAgent.keys)
            PersonAgent.keys.clear()

        if self.use_joystick:
            actions[1] = -self.x_cursor * ts
            actions[0] = -self.y_cursor
            actions[2] = 1 if -self.z_cursor > 0.1 else 0
        # print self.team
        return actions


class StationaryAgent(JRNNAgent, AgentBrain):
    """
    Simple Rotating StationaryAgent
    """

    ai = "Stationary"

    def __init__(self):
        AgentBrain.__init__(self)
        JRNNAgent.__init__(self, team=constants.OBJECT_TYPE_TEAM_1, group='StationaryAgent')

    def start(self, time, sensors):
        self.state.label = 'StationaryAgent'
        a = self.actions.get_instance()
        a[0] = a[1] = a[2] = 0
        return a

    def act(self, time, sensors, reward):
        a = self.actions.get_instance()
        a[0] = 0
        a[1] = 0.1
        a[2] = 0
        return a


class RunnerAgent(JRNNAgent, AgentBrain):
    
    ai = "Runner"

    def __init__(self):
        """
        This is the constructor - it gets called when the brain object is first created
        """
        # call the parent constructor
        AgentBrain.__init__(self)  # do not remove!
        JRNNAgent.__init__(self,team=constants.OBJECT_TYPE_TEAM_1, group="Agent")
        self.pid = PID(P=1, I=0, D=0.5, Output_max=1, Output_min=-1, Integrator_max=1, Integrator_min=-1)
        self.pid.setPoint(math.pi)
        self.sensor_angles = [150,120,90,60,30,0,330,300,270,240,210,180]
        self.sensor_angles_rad = [math.radians(x) for x in self.sensor_angles]
        self.lastturns = deque(maxlen=50)
        self.sign = lambda x: math.copysign(1, x)
        self.opposite = False
        self.oppCount = 0

    def initialize(self, init_info):
        """
        Called when the agent is first created
        """
        self.actions = init_info.actions
        self.sensors = init_info.sensors
        return True

    def start(self, time, sensors):
        """
        Take the initial sensors and return the first action
        """
        # Ignore sensors and just return the action.
        self.clearHistory()
        return self.take_action(sensors)

    def act(self, time, sensors, reward):
        """
        Take new sensors and reward from previous action and return the next action
        """
        
        return self.take_action(sensors)

    def end(self, time, reward):
        """
        Take the last reward
        """
        # Ignore the reward here as well.
        return True

    def destroy(self):
        """
        Called when the agent is destroyed
        """
        return True

    def take_action(self, sensors):
        a = self.actions.get_instance()
        obstacles = sensors[0:7]
        enemies = sensors[7:19]
        enemydistang = sensors[19:21]

        a[0] = a[1] = a[2] = 0

        mv = math.pi
        for idx, val in enumerate(enemies):
            if val > 0.8:
                mv = self.sensor_angles_rad[idx]
                a[0] = obstacles[2] * 0.8  # slows the agent down so the person can keep up

        output = self.pid.update(mv)
        a[1] = -output
        
        if self.checkEqual(self.lastturns):
            self.opposite = not self.opposite
            if self.opposite:
                self.oppCount = 0

        a[1] = -output if not self.opposite else -self.lastturns[0]
        self.lastturns.append(a[1])
        if self.opposite: self.oppCount += 1
        if self.oppCount > 50:
            self.opposite = not self.opposite
        
        # override turn and speed if the chaser gets too close
        # print "Enemy Dist/Ang: ", enemydistang
        if enemydistang[0] < (25 / constants.MAX_FOE_DISTANCE):
            a[0] = 1
            if enemydistang[1] > 0.5:
                a[1] = -0.3
            elif enemydistang[1] < 0.5:
                a[1] = 0.3
            else:
                a[1] = 0.5 if rand() < 0.5 else -0.5

        if enemydistang[0] < (10 / constants.MAX_FOE_DISTANCE):
            a[0] = 1.1
        # override turn if there are obstacles detected
        # This turns the most if there is something directly ahead and less so if the
        # obstacles are on the sides.
        if True in [x < 1 for x in obstacles[0:5]]:
            if obstacles[2] < 1:
                a[1] = 1 if rand() < 0.5 else -1
            elif obstacles[1] > obstacles[3]:
                a[1] = 0.5
            elif obstacles[1] < obstacles[3]:
                a[1] = -0.5
            elif obstacles[0] > obstacles[4]:
                a[1] = 0.2
            else:
                a[1] = -0.2

        # print "obstacles: ", obstacles, " enemies: ", enemies, " forward: ", a[0] , " turn_by: ", a[1]
        #a[0] = 0 # just for debugging the sensors. 
        return a

    def checkEqual(self, iterator):
        try:
            iterator = iter(iterator)
            first = next(iterator)
            return all(first == rest for rest in iterator)
        except StopIteration:
            return False

    def set_display_hint(self):
        """
        Set the display hint above the agents head. 
        """
        display_hint = constants.getDisplayHint()
        if display_hint:
            if display_hint == 'damage':
                self.state.label = "{0}".format(get_environment().get_damage(self))
            else:
                self.state.label = '?'
        else:
            if self.state.label:
                self.state.label = ""

def createInputMapping():
    """
    Create key bindings to control the agent.
    """
    # create an io map
    ioMap = PyIOMap()

    # tell the agent which key is pressed
    def control(key):
        def closure():
            PersonAgent.keys.add(key)
        return closure

    def joystick_action():
        sim_context = getSimContext()

        if sim_context.JoysticksActive():
            x_cursor = sim_context.getJoystickAxisPosition(JoystickAxis.AXIS_X)
            y_cursor = sim_context.getJoystickAxisPosition(JoystickAxis.AXIS_Y)
            z_cursor = sim_context.getJoystickAxisPosition(JoystickAxis.AXIS_Z)
            PersonAgent.x_cursor = x_cursor
            PersonAgent.y_cursor = y_cursor
            PersonAgent.z_cursor = z_cursor
            PersonAgent.use_joystick = True

    # bind arrow keys to control agent.
    ioMap.ClearMappings()
    ioMap.BindKey("KEY_RIGHT", "onHold", control('RHT'))
    ioMap.BindKey("KEY_LEFT", "onHold", control('LFT'))
    ioMap.BindKey("KEY_UP", "onHold", control('FWD'))
    ioMap.BindKey("KEY_DOWN", "onHold", control('BCK'))
    ioMap.BindKey("KEY_SPACE", "onPress", control('FRE'))
    ioMap.BindJoystickAction("moveXaxis", joystick_action)
    ioMap.BindJoystickAction("moveYaxis", joystick_action)
    ioMap.BindJoystickAction("moveZaxis", joystick_action)
    return ioMap
