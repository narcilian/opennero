from OpenNero import *  # import the OpenNERO C++ API, such as Vector3f

import common
import constants
import json

import copy
import math
import random


class AgentState:
    """
    State that we keep for each agent
    """
    def __init__(self, agent):
        self.id = agent.state.id
        self.agent = agent
        self.pose = (0, 0, 0)  # current x, y, heading
        self.prev_pose = (0, 0, 0)
        self.startheight = agent.state.position.z
        self.initial_position = Vector3f(0, 0, 0)
        self.initial_rotation = Vector3f(0, 0, 0)
        self.total_damage = 0
        self.curr_damage = 0

    def __str__(self):
        x, y, h = self.pose
        px, py, ph = self.prev_pose
        return 'agent { id: %d, pose: (%.02f, %.02f, %.02f), prev_pose: (%.02f, %.02f, %.02f) }' % \
            (self.id, x, y, h, px, py, ph)

    def randomize(self):
        #pass
        dx = random.randrange(constants.XDIM / 20) - constants.XDIM / 40
        dy = random.randrange(constants.XDIM / 20) - constants.XDIM / 40
        self.initial_position.x = module.getMod().spawn_x[self.agent.get_team()] + dx
        self.initial_position.y = module.getMod().spawn_y[self.agent.get_team()] + dy
        self.prev_pose = self.pose = (self.initial_position.x,
                                      self.initial_position.y,
                                      self.initial_rotation.z)

    def setInitialConditions(self):
        # This is called so that the initial_postiion and initial_rotation are set.
        self.initial_position = copy.copy(self.agent.state.position)
        self.initial_rotation = copy.copy(self.agent.state.rotation)

    def reset_pose(self, position, rotation):
        self.prev_pose = self.pose = (position.x, position.y, rotation.z)

    def update_damage(self):
        """
        Update the damage for an agent, returning the current damage.
        """
        self.total_damage += self.curr_damage
        damage = self.curr_damage
        self.curr_damage = 0
        return damage

    def update_pose(self, move_by, turn_by):
        dist = constants.MAX_MOVEMENT_SPEED * move_by
        heading = common.wrap_degrees(self.agent.state.rotation.z, turn_by)
        x = self.agent.state.position.x + dist * math.cos(math.radians(heading))
        y = self.agent.state.position.y + dist * math.sin(math.radians(heading))

        self.prev_pose = self.pose
        self.pose = (x, y, heading)

        # try to update position
        pos = copy.copy(self.agent.state.position)
        pos.x = x
        pos.y = y
        if pos.z != self.startheight:  # fix height issues
            pos.z = self.startheight
            # print "Reset Height"
        self.agent.state.position = pos

        # try to update rotation
        rot = copy.copy(self.agent.state.rotation)
        rot.z = heading
        self.agent.state.rotation = rot


class JRNNEnvironment(Environment):
    def __init__(self):
        """
        Create the environment
        """
        Environment.__init__(self)

        # # define the action space with two actions
        # action_info = FeatureVectorInfo()
        # action_info.add_discrete(-1, 1)  # -1, 0, or 1 (first action)
        # action_info.add_discrete(-1, 1)  # -1, 0, or 1 (second action)

        # # define the observation space
        # observation_info = FeatureVectorInfo()
        # observation_info.add_continuous(0, 1)  # between 0 and 100

        # define the reward space
        # reward_info = FeatureVectorInfo()
        # reward_info.add_continuous(0, 100)  # between 0 and 100

        self.curr_id = 0
        self.MAX_DIST = math.hypot(constants.XDIM, constants.YDIM)

        self.states = {}
        self.teams = dict((t, set()) for t in constants.TEAMS)
        self.agents_to_load = {}
        
        self.script = "jrnnexp1/menu.py"

        self.allowShooting = False

        # we only want to compute the center of mass of foes once per tick
        self.foe_center = dict((team, None) for team in constants.TEAMS)
        # we need to know when to update the foe center
        self.foe_center_cache = dict((team, {}) for team in constants.TEAMS)
        # self.agent_info = AgentInitInfo(observation_info, action_info, reward_info)
        print 'Initialized TestEnvironment'
        # print self.agent_info

    def kill_agent(self, agent):
        agent.dead = True

    def kill_all_agents(self, team):
        for agent in list(self.teams[team]):
            self.kill_agent(agent)

    def remove_all_agents(self, team):
        for agent in list(self.teams[team]):
            self.remove_agent(agent)

    def write_agent_histories(self, fileh):
        histories = {}
        for agent, state in self.states.items():
            start = {"initial_position": {"x": state.initial_position.x, "y": state.initial_position.y, "z": state.initial_position.z},
                     "initial_rotation": {"x": state.initial_rotation.x, "y": state.initial_rotation.y, "z": state.initial_rotation.z}}
            history = agent.getHistory()
            history.insert(0, start)
            histories[agent.ai] = copy.copy(history)
            agent.clearHistory()

        json.dump(histories, fileh)

    def write_agent_trainingData(self, fileh):
        for agent, state in self.states.items():
            if agent.ai == "Person":
                trainingData = agent.getTrainingData()
                json.dump(trainingData, fileh)
                agent.clearTrainingData()

    def get_network_agent(self):
        for agent, state in self.states.items():
            if agent.ai == "Network":
                return agent

    def remove_agent(self, agent):
        common.removeObject(agent.state.id)
        try:
            self.teams[agent.get_team()].discard(agent)
            if agent in self.states:
                self.states.pop(agent)
        except:
            pass

    def get_agent_by_id(self, id):
        for agent in self.states:
            if agent.state.id == id:
                return agent

    def reset(self, agent):
        """
        Reset the environment to its initial state
        """
        return True

    def get_agent_info(self, agent):
        for a in constants.WALL_RAY_SENSORS:
            agent.add_sensor(RaySensor(
                math.cos(math.radians(a)), math.sin(math.radians(a)), 0, 50,
                constants.OBJECT_TYPE_OBSTACLE,
                True))
        # for a0, a1 in constants.FLAG_RADAR_SENSORS:
        #     agent.add_sensor(RadarSensor(
        #             a0, a1, -90, 90, constants.MAX_VISION_RADIUS,
        #             constants.OBJECT_TYPE_FLAG,
        #             False))
        for a0, a1 in constants.OBS_RADAR_SENSORS:
            agent.add_sensor(RadarSensor(
                a0, a1, -90, 90, 80,
                constants.OBJECT_TYPE_OBSTACLE,
                True))

        for a0, a1 in constants.ENEMY_RADAR_SENSORS:
            sense = constants.OBJECT_TYPE_TEAM_0
            if agent.get_team() == sense:
                sense = constants.OBJECT_TYPE_TEAM_1
            agent.add_sensor(RadarSensor(
                a0, a1, -90, 90, self.MAX_DIST,  # constants.MAX_VISION_RADIUS,
                sense,
                True))
        return agent.info

    def getFriendFoe(self, agent):
        """
        Returns sets of all friend agents and all foe agents.
        """
        my_team = agent.get_team()
        other_team = constants.OBJECT_TYPE_TEAM_1
        if my_team == other_team:
            other_team = constants.OBJECT_TYPE_TEAM_0
        return self.teams[my_team], self.teams[other_team]

    def distance(self, a, b):
        """
        Returns the distance between positions (x-y tuples) a and b.
        """
        return math.hypot(a[0] - b[0], a[1] - b[1])

    def angle(self, a, b):
        """
        Returns the relative angle from a looking towards b, in the interval
        [-180, +180]. a needs to be a 3-tuple (x, y, heading) and b needs to be
        an x-y tuple.
        """
        if self.distance(a, b) == 0:
            return 0
        rh = math.degrees(math.atan2(b[1] - a[1], b[0] - a[0])) - a[2]
        if rh < -180:
            rh += 360
        if rh > 180:
            rh -= 360
        return rh

    def nearest(self, loc, agents):
        """
        Returns the nearest agent to a particular location.
        """
        # TODO: this needs to only be computed once per tick, not per agent
        if not agents:
            return None
        nearest = None
        min_dist = self.MAX_DIST * 5
        for agent in agents:
            d = self.distance(loc, self.get_state(agent).pose)
            if 0 < d < min_dist:
                nearest = agent
                min_dist = d
        return nearest

    def get_state(self, agent):
        """
        Returns the state of an agent
        """
        if agent not in self.states:
            newState = AgentState(agent)
            newState.setInitialConditions()  # This is done so that the initial postions are set.
            self.states[agent] = newState
            self.teams[agent.get_team()].add(agent)

        return self.states[agent]

    def step(self, agent, actions):
        """
        Adjust the world and the agent based on its action
        @return the agent's reward
        """
        # If either action is non-zero, then change the position and orientation
        # of the agent.
        # if actions[0] != 0 or actions[1] != 0:
        #     agent.state.position = Vector3f(agent.state.position.x + actions[0],
        #                                   agent.state.position.y + actions[1],
        #                                   agent.state.position.z)
        #     agent.state.rotation = Vector3f(agent.state.rotation.x,
        #                                   agent.state.rotation.y,
        #                                   degrees(atan2(actions[1], actions[0])))
        # return 0

        # Need to check to see if there are any agents that need things to be loaded.
        # This is only used for the replay agents right now.
        parameters = self.agents_to_load.get(agent.state.id)
        if parameters != None:
            print "Loading Paramters for Agent"
            del self.agents_to_load[agent.state.id]
            success = False
            if 'agentname' in parameters and 'filePath' in parameters:
                if hasattr(agent, 'LoadHistoryFile'):
                    success = agent.LoadHistoryFile(parameters['filePath'], parameters['agentname'])

            if not success:
                self.remove_agent(agent)
                
        state = self.get_state(agent)

        # set the display hint
        if hasattr(agent, 'set_display_hint'):
            agent.set_display_hint()

        move_by = 0.0
        turn_by = 0.0
        fire = 0
        if agent.ai == "Replay" and agent.historyLoaded:
            savedactions = agent.GetCurrentStep()
            move_by = savedactions['move_by']
            turn_by = savedactions['turn_by']
            if 'fire' in savedactions:
                fire = savedactions['fire']
        else:
            move_by = actions[0]
            turn_by = math.degrees(constants.MAX_TURNING_RATE * actions[1])
            fire = actions[2]
        
        #print agent.ai, " Move By: ", move_by, " Turn By: ", turn_by
        state.update_damage()  # This might move to a calculate reward function.
        if agent.ai != "Stationary" and agent.ai != "Replay":
            if self.allowShooting == True and agent.ai == "Person":
                agent.writeHistory({"move_by": move_by, "turn_by": turn_by, "fire": fire})
            else:
                agent.writeHistory({"move_by": move_by, "turn_by": turn_by})

        if move_by != 0:
            self.set_animation(agent, state, 'run')
            # delay = getSimContext().delay
            agent.state.animation_speed = move_by * constants.ANIMATION_RATE
        else:
            self.set_animation(agent, state, 'stand')

        state.update_pose(move_by, turn_by)
        numHits = 0

        if self.allowShooting and fire:
            numHits = self.attemptShot(agent)

        if numHits > 0:
            pass
            #print numHits
        
        reward = self.calculate_reward(agent, actions, fire)
        return reward

    def calculate_reward(self, agent, action, fire):
        reward = agent.info.reward.get_instance()

        state = self.get_state(agent)
        friends, foes = self.getFriendFoe(agent)

        R = dict((f, 0) for f in constants.FITNESS_DIMENSIONS)

        foe = self.nearest(state.pose, foes)
        if foe:
            d = self.distance(self.get_state(foe).pose, state.pose)
            # R[constants.FITNESS_APPROACH_ENEMY] = -d * d
            R[constants.FITNESS_APPROACH_ENEMY] = 1 if d < 45 else 0

        if self.allowShooting and fire:
            numHits = self.attemptShot(agent)
            R[constants.FITNESS_HIT_TARGET] = numHits

        if len(reward) == 1:
            for i, f in enumerate(constants.FITNESS_DIMENSIONS):
                reward[0] += self.reward_weights[f] * R[f] / constants.FITNESS_SCALE.get(f, 1.0)
                #print f, self.reward_weights[f], R[f] / constants.FITNESS_SCALE.get(f, 1.0)
        else:
            for i, f in enumerate(constants.FITNESS_DIMENSIONS):
                reward[i] = R[f]

        return reward
    
    def set_animation(self, agent, state, animation):
        """
        Sets current animation
        """
        if agent.state.animation != animation:
            agent.state.animation = animation

    def target(self, agent):
        """
        Returns the nearest foe in a 2-degree cone from an agent.
        """
        friends, foes = self.getFriendFoe(agent)
        if not foes:
            return None
        pose = self.get_state(agent).pose
        min_f = None
        min_v = None
        for f in foes:
            p = self.get_state(f).pose
            fd = self.distance(pose, p)
            fh = abs(self.angle(pose, p))
            if fh <= 2 and fd < constants.MAX_SHOT_RADIUS:
                v = fd / math.cos(math.radians(fh * 20))
                if min_v is None or v < min_v:
                    min_f = f
                    min_v = v
        return min_f

    def attemptShot(self, agent):
        targ = self.target(agent)
        hit = 0

        color = constants.TEAM_LABELS[agent.get_team()]
        if color == 'red':
            color = Color(255, 255, 0, 0)
        elif color == 'blue':
            color = Color(255, 0, 0, 255)
        else:
            color = Color(255, 255, 255, 0)
        wall_color = Color(128, 0, 255, 0)

        if targ != None:
            source_pos = agent.state.position
            # target_pos = self.getTargetPos(agent)
            target_pos = targ.state.position
            source_pos.z = source_pos.z + 5
            target_pos.z = target_pos.z + 5

            obstacles = getSimContext().findInRay(
                    source_pos,
                    target_pos,
                    constants.OBJECT_TYPE_OBSTACLE,
                    True,
                    wall_color,
                    color)

            if len(obstacles) == 0:
                self.get_state(targ).curr_damage += 1
                # add reward part here. 
                hit += 1
        else:
            source_pos = agent.state.position
            target_pos = self.getTargetPos(agent)
            # target_pos = targ.state.position
            source_pos.z = source_pos.z + 5
            target_pos.z = target_pos.z + 5

            my_team = agent.get_team()
            other_team = constants.OBJECT_TYPE_TEAM_1
            if my_team == other_team:
                other_team = constants.OBJECT_TYPE_TEAM_0

            targetsHit = getSimContext().findInRay(
                source_pos,
                target_pos,
                other_team,
                True,
                color,
                wall_color)

        return hit

    def getTargetPos(self, agent):
        state = self.get_state(agent)
        heading = state.pose[2]
        retVec = Vector3f(0,0,0)
        tarX = agent.state.position.x + constants.MAX_SHOT_RADIUS * math.cos(math.radians(heading))
        tarY = agent.state.position.y + constants.MAX_SHOT_RADIUS * math.sin(math.radians(heading))
        retVec.x = tarX
        retVec.y = tarY
        retVec.z = 0
        return retVec

    def sense(self, agent, default_observation):
        """
        Figure out what the agent's input (observation) is
        @return the agent's observation
        """

        friends, foes = self.getFriendFoe(agent)
        if not foes:
            return default_observation

        ax, ay = agent.state.position.x, agent.state.position.y
        cx, cy = self.get_foe_center(agent, foes)
        fd = self.distance((ax, ay), (cx, cy))
        ah = agent.state.rotation.z
        fh = self.angle((ax, ay, ah), (cx, cy)) + 180
        if fd <= constants.MAX_FOE_DISTANCE:
            default_observation[constants.SENSOR_INDEX_ENEMY_ANG_DIST[0]] = fd / constants.MAX_FOE_DISTANCE
            default_observation[constants.SENSOR_INDEX_ENEMY_ANG_DIST[1]] = fh / 360.0
        return default_observation

    def get_foe_center(self, agent, foes):
        ''' get the x, y position of the center of mass of the foes group '''
        id = agent.state.id
        step = agent.step
        team = agent.get_team()
        # only recompute the center of mass once we find an agent that goes
        # to the next step since the last computation
        if self.foe_center[team] is None or step != self.foe_center_cache[team].get(id, step):
            n = float(len(foes))
            cx, cy = 0, 0
            for f in foes:
                fx, fy = f.state.position.x, f.state.position.y
                cx += fx / n
                cy += fy / n
            self.foe_center[team] = (cx, cy)
            self.foe_center_cache[team].clear()
        self.foe_center_cache[team][id] = step
        return self.foe_center[team]

    def get_damage(self, agent):
        damage = self.get_state(agent).total_damage
        return damage

    def is_active(self, agent):
        """
        Return whether the agent should act or not
        """
        return True

    def is_episode_over(self, agent):
        """
        Figure out if the agent should 'die' and 'respawn'
        """
        return agent.dead

    def cleanup(self):
        """
        Perform any last-minute cleanup tasks
        """

        common.killScript(self.script)
        return True
