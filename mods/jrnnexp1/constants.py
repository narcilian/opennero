import math
import itertools
import random

# Dimensions of the arena
XDIM = 800
YDIM = 800

# Height and width of walls
WIDTH = 4
HEIGHT = 20
OFFSET = -HEIGHT / 2

# maximum movement speed in world units
MAX_MOVEMENT_SPEED = 1

# maximum turning rate in radians
MAX_TURNING_RATE = 0.2

# maximum vision radius for most sensors
MAX_VISION_RADIUS = 300

# maximum shot radius
MAX_SHOT_RADIUS = 100

# maximum distance to center of friend group
MAX_FRIEND_DISTANCE = 15.0

# maximum distance to center of foe group
MAX_FOE_DISTANCE = 600.0

# rate of animation at full speed (in frames per second)
ANIMATION_RATE = 100

# Population size
pop_size = 50

# number of steps per lifetime
DEFAULT_LIFETIME = 1000
DEFAULT_HITPOINTS = 20

# default value of explore/exploit slider (x out of 100)
DEFAULT_EE = 10

# default speedup setting (there will be a 0.1 * (100-DEFAULT_SPEEDUP) / 100 second delay between AI steps)
DEFAULT_SPEEDUP = 100

OBJECT_TYPE_OBSTACLE  = (1 << 0) # object type for walls
OBJECT_TYPE_TEAM_0 = (1 << 1) # object type for team 1
OBJECT_TYPE_TEAM_1 = (1 << 2) # object type for team 2 and turrets during training
OBJECT_TYPE_FLAG = (1 << 3) # object type for the flag
OBJECT_TYPE_LEVEL_GEOM = 0 # object type for the level geometry

TEAMS = (OBJECT_TYPE_TEAM_0, OBJECT_TYPE_TEAM_1)
TEAM_LABELS = {OBJECT_TYPE_TEAM_0: 'blue', OBJECT_TYPE_TEAM_1: 'red'}

############################
### SENSOR CONFIGURATION ###
############################

N_SENSORS = 0  # start at 0, add each sensor bank's length

# Wall Ray Sensors
WALL_RAY_SENSORS = [90, 45, 0, -45, -90]
SENSOR_INDEX_WALL_RAY = [i for i in range(N_SENSORS, N_SENSORS + len(WALL_RAY_SENSORS))]
N_SENSORS += len(WALL_RAY_SENSORS)

# Forward Obstacle Radar
OBS_RADAR_SENSORS = [(15, -5), (5, -15)]
SENSOR_INDEX_OBS_RADAR = [i for i in range(N_SENSORS, N_SENSORS + len(OBS_RADAR_SENSORS))]
N_SENSORS += len(OBS_RADAR_SENSORS)

# EnemyRadarSensor
# ENEMY_RADAR_SENSORS = [(90, 12), (18, -3), (3, -18), (-12, -90), (-87, 87)]
ENEMY_RADAR_SENSORS = [(165, 135), (135, 105), (105, 75), (75, 45), (45, 15), (15, -15),\
                        (-15, -45), (-45, -75), (-75, -105), (-105, -135), (-135, -165), \
                        (-165, 165)]
SENSOR_INDEX_ENEMY_RADAR = [i for i in range(N_SENSORS, N_SENSORS + len(ENEMY_RADAR_SENSORS))]
N_SENSORS += len(ENEMY_RADAR_SENSORS)

#2 Enemy sensors - Distance and angle to enemy center of mass - Useful in chaser
ENEMY_ANG_DIST_SENSORS = [(0, MAX_VISION_RADIUS), (0, 360)]
SENSOR_INDEX_ENEMY_ANG_DIST = [i for i in range(N_SENSORS, N_SENSORS + len(ENEMY_ANG_DIST_SENSORS))]
N_SENSORS += len(ENEMY_ANG_DIST_SENSORS)

# Flag Radar Sensors
# FLAG_RADAR_SENSORS = [(90, 12), (18, -3), (3, -18), (-12, -90), (-87, 87)]
# SENSOR_INDEX_FLAG_RADAR = [i for i in range(N_SENSORS, N_SENSORS+len(FLAG_RADAR_SENSORS))]
# N_SENSORS += len(FLAG_RADAR_SENSORS)

# 2 friend sensors - distance and angle to friend center of mass
# FRIEND_RADAR_SENSORS = [(0,MAX_FRIEND_DISTANCE),(0,360)]
# SENSOR_INDEX_FRIEND_RADAR = [i for i in range(N_SENSORS, N_SENSORS+len(FRIEND_RADAR_SENSORS))]
# N_SENSORS += len(FRIEND_RADAR_SENSORS)

# # 1 targeting sensor - becomes 1 if and only iff the agent is looking at a target
# # Flag Radar Sensors
# TARGETING_SENSORS = [(-2,2)]
# SENSOR_INDEX_TARGETING = [i for i in range(N_SENSORS, N_SENSORS+len(TARGETING_SENSORS))]
# N_SENSORS += len(TARGETING_SENSORS)

# Number of actions
N_ACTIONS = 2

# Network bias value
NEAT_BIAS = 0.3

# Spawn Points
SPAWN_POINTS = []
for i in range(1, 6):
    for j in range(1, 6):
        if i != 3 and j != 3:
            point = (XDIM * (i / 6.0), YDIM * (j / 6.0))
            SPAWN_POINTS.append(point)


def getRandomSpawnPoint():
    index = random.randrange(len(SPAWN_POINTS))
    return SPAWN_POINTS[index]

#############################
### FITNESS CONFIGURATION ###
#############################

FITNESS_APPROACH_ENEMY = 'Approach enemy'
FITNESS_AVOID_OBSTACLES = 'Avoid obstacles'
FITNESS_HIT_TARGET = 'Hit target'

FITNESS_DIMENSIONS = [FITNESS_APPROACH_ENEMY, FITNESS_AVOID_OBSTACLES, FITNESS_HIT_TARGET]

# FITNESS_STAND_GROUND = 'Stand ground'
# FITNESS_STICK_TOGETHER = 'Stick together'
# FITNESS_APPROACH_ENEMY = 'Approach enemy'
# FITNESS_APPROACH_FLAG = 'Approach flag'
# FITNESS_HIT_TARGET = 'Hit target'
# FITNESS_AVOID_FIRE = 'Avoid fire'
# FITNESS_DIMENSIONS = [FITNESS_STAND_GROUND, FITNESS_STICK_TOGETHER,
#     FITNESS_APPROACH_ENEMY, FITNESS_APPROACH_FLAG, FITNESS_HIT_TARGET,
#     FITNESS_AVOID_FIRE]

FITNESS_INDEX = dict([(f,i) for i,f in enumerate(FITNESS_DIMENSIONS)])

SQ_DIST_SCALE = math.hypot(XDIM, YDIM) / 2.0
# FITNESS_SCALE = {
#     FITNESS_STAND_GROUND: SQ_DIST_SCALE,
#     FITNESS_STICK_TOGETHER: SQ_DIST_SCALE,
#     FITNESS_APPROACH_ENEMY: SQ_DIST_SCALE,
#     FITNESS_APPROACH_FLAG: SQ_DIST_SCALE,
#     }

FITNESS_SCALE = {
    FITNESS_APPROACH_ENEMY: SQ_DIST_SCALE,
    }

#DISPLAY_HINTS = itertools.cycle([None, 'fitness', 'time alive', 'champion', 'hit points', 'id', 'species id'])
DISPLAY_HINTS = itertools.cycle([None, 'damage'])
DISPLAY_HINT = DISPLAY_HINTS.next()

def getDisplayHint():
    return DISPLAY_HINT

def nextDisplayHint():
    global DISPLAY_HINT
    DISPLAY_HINT = DISPLAY_HINTS.next()
    return DISPLAY_HINT
